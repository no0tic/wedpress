<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file definisce le seguenti configurazioni: impostazioni MySQL,
 * Prefisso Tabella, Chiavi Segrete, Lingua di WordPress e ABSPATH.
 * E' possibile trovare ultetriori informazioni visitando la pagina: del
 * Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Editing wp-config.php}. E' possibile ottenere le impostazioni per
 * MySQL dal proprio fornitore di hosting.
 *
 * Questo file viene utilizzato, durante l'installazione, dallo script
 * di creazione di wp-config.php. Non � necessario utilizzarlo solo via
 * web,� anche possibile copiare questo file in "wp-config.php" e
 * rimepire i valori corretti.
 *
 * @package WordPress
 */
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
    include( dirname( __FILE__ ) . '/local-config.php' );
}
// ** Impostazioni MySQL - E? possibile ottenere questoe informazioni
// ** dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'ebdb');

/** Nome utente del database MySQL */
define('DB_USER', 'wedpress');

/** Password del database MySQL */
define('DB_PASSWORD', 'w3dpr3ss');

/** Hostname MySQL  */
define('DB_HOST', 'aavbbn8710utqu.cxxksdffdvsj.eu-west-1.rds.amazonaws.com');

/** Charset del Database da utilizare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha
idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * E' possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * E' possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ci� forzer� tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Om+ooNrHj3~2@`-qGa}!yq-=|s.!.:9]xK!2<c^RH!%^~eB5B43%+c-LhMlKU+>+');
define('SECURE_AUTH_KEY',  '4h`p~1XxDNPNeDK-d=rSnrI0$R>>q,--[-rZ,{I K9qJCWqFMt[@zv_SBRAG|@:#');
define('LOGGED_IN_KEY',    'l~K97_.)*vMKfFv+cp:EG~wX>uP4}(D-gs#>-F[%;|{zY)NS(Mz#TU E+=-un ]G');
define('NONCE_KEY',        '0*=myg2S7SPt%Z{ `FJpIxrU#FQoRJ~|fbD&dqqxY8s$Z2aEu.w;#>S;Bs]Pqj+f');
define('AUTH_SALT',        '-d}Rzm<d9yJLvY4aq!DPAFG7}B[EZ14Z!-+/_e?U_CgYSK4ebJv6<|WNUyY=Tg?4');
define('SECURE_AUTH_SALT', '`w:Qek2*pW9oJJ9?1<[d88=8Um+09?kQ]2@-,,I:p*mSscdl_/*z8}-oXx>b4L/X');
define('LOGGED_IN_SALT',   'cZm&JJL[i`V2bO@NYNkR;v?wXE@1ZKb/v!+NrK4&)@i@/{;.gAbF1|{t96p!v:`F');
define('NONCE_SALT',       'f9O?T0!7EMcoU^~VItr=T[p=zf;N|S%6a$KMXBe|spU>]/3#%S^xxte|r;u-%:9~');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress .
 *
 * E' possibile avere installazioni multiple su di un unico database if you give each a unique
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalit� di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * E' fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all'interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta lle variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
