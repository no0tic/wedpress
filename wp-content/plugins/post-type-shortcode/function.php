<?php
/*
Plugin Name: Post type and shortcode
Version: 1.0.1
Author: Beautheme
Author URI: http://www.beautheme.com
License: GPLv2
*/

// Register Post type
add_action('init', 'event_post_type');
add_action('init', 'gallery_post_type');
add_action('init', 'slider_post_type');
add_action('init', 'couple_post_type');

function event_post_type()
{
    $labels = array(
        'name' => _x('Event', 'post type general name', 'beautheme'),
        'singular_name' => _x('Event', 'post type singular name', 'beautheme'),
        'add_new' => _x('Add New', 'event', 'beautheme'),
        'add_new_item' => __('Add New Event', 'beautheme'),
        'edit_item' => __('Edit Event', 'beautheme'),
        'new_item' => __('New Event', 'beautheme'),
        'all_items' => __('All Event', 'beautheme'),
        'view_item' => __('View Event', 'beautheme'),
        'search_items' => __('Search Event', 'beautheme'),
        'not_found' =>  __('No Event Found', 'beautheme'),
        'not_found_in_trash' => __('No Event Found in Trash', 'beautheme'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'event-item', 'with_front' => true),
        'query_var' => true,
        'show_in_nav_menus'=> false,
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
    );
    register_post_type( 'event' , $args );
}

function gallery_post_type()
{
    $labels = array(
        'name' => _x('Gallery', 'post type general name', 'beautheme'),
        'singular_name' => _x('Gallery', 'post type singular name', 'beautheme'),
        'add_new' => _x('Add New', 'gallery', 'beautheme'),
        'add_new_item' => __('Add New Gallery', 'beautheme'),
        'edit_item' => __('Edit Gallery', 'beautheme'),
        'new_item' => __('New Gallery', 'beautheme'),
        'all_items' => __('All Gallery', 'beautheme'),
        'view_item' => __('View Gallery', 'beautheme'),
        'search_items' => __('Search Gallery', 'beautheme'),
        'not_found' =>  __('No Gallery Found', 'beautheme'),
        'not_found_in_trash' => __('No Gallery Found in Trash', 'beautheme'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'gallery-item', 'with_front' => true),
        'query_var' => true,
        'show_in_nav_menus'=> false,
        'supports' => array('title', 'editor', 'page-attributes', 'thumbnail')
    );
    register_post_type( 'gallery' , $args );
    register_taxonomy('gallery-category',
        array('gallery'),
        array(
            'hierarchical' => true,
            'label' => 'Gallery Categories',
            'singular_label' => 'Gallery Categories',
            'rewrite' => true,
            'query_var' => true,
            'show_in_nav_menus'=> false
        ));
}

function slider_post_type()
{
    $labels = array(
        'name' => _x('Slider( Home )', 'post type general name', 'beautheme'),
        'singular_name' => _x('Slider( Home )', 'post type singular name', 'beautheme'),
        'add_new' => _x('Add New', 'slider', 'beautheme'),
        'add_new_item' => __('Add New Slider', 'beautheme'),
        'edit_item' => __('Edit Slider', 'beautheme'),
        'new_item' => __('New Slider', 'beautheme'),
        'all_items' => __('All Slider', 'beautheme'),
        'view_item' => __('View Slider', 'beautheme'),
        'search_items' => __('Search Slider', 'beautheme'),
        'not_found' =>  __('No Slider Found', 'beautheme'),
        'not_found_in_trash' => __('No Slider Found in Trash', 'beautheme'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'slider-item', 'with_front' => true),
        'query_var' => true,
        'show_in_nav_menus'=> false,
        'exclude_from_search' => true,
        'supports' => array('title', 'editor', 'page-attributes','thumbnail')
    );
    register_post_type( 'slider' , $args );
}

function couple_post_type()
{
    $labels = array(
        'name' => _x('Couple', 'post type general name', 'beautheme'),
        'singular_name' => _x('Couple', 'post type singular name', 'beautheme'),
        'add_new' => _x('Add New', 'couple', 'beautheme'),
        'add_new_item' => __('Add New Couple', 'beautheme'),
        'edit_item' => __('Edit Couple', 'beautheme'),
        'new_item' => __('New Couple', 'beautheme'),
        'all_items' => __('All Couple', 'beautheme'),
//        'view_item' => __('View Couple', 'beautheme'),
        'search_items' => __('Search Couple', 'beautheme'),
        'not_found' =>  __('No Couple Found', 'beautheme'),
        'not_found_in_trash' => __('No Couple Found in Trash', 'beautheme'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'couple-item', 'with_front' => true),
        'query_var' => true,
        'show_in_nav_menus'=> false,
        'supports' => array('title', 'editor','thumbnail', 'excerpt')
    );
    register_post_type( 'couple' , $args );
}
?>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 8/2/14
 * Time: 11:02 PM
 * To change this template use File | Settings | File Templates.
 */
/*
 * Colums
 *
 *
 * */
function beau_row($atts, $content = null)
{
    $out = '<div class="row-fluid">' . beau_do_shortcode($content, TRUE) . '</div>';
    return $out;
}

function beau_columns($atts, $content = null, $code = '')
{
    $columns = array();
    $columns['one_half'] = 'span6';
    $columns['one_third'] = 'span4';
    $columns['two_third'] = 'span8';
    $columns['one_fourth'] = 'span3';
    $columns['three_fourth'] = 'span9';
    $columns['nested_one_half'] = 'span6';
    $columns['nested_one_third'] = 'span4';
    $columns['nested_two_third'] = 'span8';
    $columns['nested_one_fourth'] = 'span3';
    $columns['nested_three_fourth'] = 'span9';
    $codetoclass = $columns[$code];

    $out = '<div class="st-column ' . $codetoclass . '">' . beau_do_shortcode($content, TRUE) . '</div>';
    return $out;
}

add_shortcode('row', 'beau_row');
add_shortcode('one_half', 'beau_columns');
add_shortcode('one_third', 'beau_columns');
add_shortcode('two_third', 'beau_columns');
add_shortcode('one_fourth', 'beau_columns');
add_shortcode('three_fourth', 'beau_columns');
add_shortcode('nested_row', 'beau_row');
add_shortcode('nested_one_half', 'beau_columns');
add_shortcode('nested_one_third', 'beau_columns');
add_shortcode('nested_two_third', 'beau_columns');
add_shortcode('nested_one_fourth', 'beau_columns');
add_shortcode('nested_three_fourth', 'beau_columns');
/*
 * Heading
 * */
function beau_heading($atts, $content = null)
{
    extract(shortcode_atts(array(
        'align' => 'left'
    ), $atts));
    $attstoclass = '';
    if ($align == 'left' || $align == 'center' || $align == 'right') $attstoclass = 'text-' . $align;
    $out = '<h3 class="st-heading ' . $attstoclass . '"><span>' . strip_tags($content) . '</span><div></div></h3>';
    return $out;
}
add_shortcode('heading', 'beau_heading');
///**
// Shortcode Couple
// * */
function beau_couple($atts, $content = null, $code = '')
{
    global $post, $imgsize, $style;
    ?>
    <section class="beau-couple-slide">
        <div class="row-fluid">
            <?php
            $args = array(
                'post_type' => 'couple',
                'posts_per_page' => 2,
            );
            $loop = new WP_Query($args);
            ob_start();
            switch ($style):
                case 1:
                    $img_style = "";
                    break;
                case 2:
                    $img_style = "-style2";
                    break;
            endswitch;
            if ($loop->have_posts()):?>
                <?php $i = 0;
                while ($loop->have_posts()) : $loop->the_post();
                    $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    $age = get_post_meta(get_the_ID(), '_couple_age', TRUE);
                    $facebook = get_post_meta(get_the_ID(), '_couple_facebook', TRUE);
                    $twitter = get_post_meta(get_the_ID(), '_couple_twitter', TRUE);
                    $google = get_post_meta(get_the_ID(), '_couple_google', TRUE);
                    $pinterest = get_post_meta(get_the_ID(), '_couple_pinterest', TRUE);
                    if ($i == 0) {
                        $pos = "left";
                        $animate="right";
                    } else {
                        $pos = "right";
                        $animate="left";
                    }
                    ?>
                    <div class="span6 couple-slide-<?php echo $pos; ?> beau-animated move-to-<?php echo $animate; ?>">
                        <div class="couple-img">
                            <?php beau_resizer($featuredID[0], $imgsize['couple']['w'], $imgsize['couple']['h'], true, get_the_title(get_the_ID())); ?>
                        </div>
                        <div class="couple-text">
                            <h2 class="couple-name"><?php the_title(); ?></h2>
                            <div class="couple-old"><?php echo $age; ?> year old</div>
                            <div class="couple-content"><?php the_content(); ?></div>
                            <div class="couple-social">
                                <ul class="inline">
                                    <li><a href="<?php echo $facebook; ?>"><i class="icon-facebook"></i></a></li>
                                    <li><a href="<?php echo $twitter; ?>"><i class="icon-twitter"></i></a></li>
                                    <li><a href="<?php echo $google; ?>"><i class="icon-linkedin"></i></a></li>
                                    <li><a href="<?php echo $pinterest; ?>"><i class="icon-google-plus"></i></a></li>
                                </ul>
                            </div>
                            <div class="couple-close"></div>
                        </div>
                    </div>
                    <?php
                    $i++;
                endwhile;?>
                <?php
                wp_reset_postdata();
            endif;?>
            <div class="couple-img-elipse">
                <img src="<?php echo BEAU_IMAGES . '/icons/elipse.png' ?>" alt="<?php the_title();?>">
            </div>
        </div>
        <div class="beau-couple-shadow"></div>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('couple', 'beau_couple');
/*
Shortcode Tweets
*/
function beau_tweets($atts, $content = null, $code = '')
{
    extract(shortcode_atts(array(
        'class' => '',
    ), $atts));
    $author=get_post_meta(get_the_ID(), '_page_tweet_author', TRUE);
    ?>
    <h2><?php echo get_post_meta(get_the_ID(), '_page_tweet_text', TRUE); ?></h2>
    <a href="https://api.twitter.com/1/lists/statuses.json?owner_id=492517635670568960&amp;per_page=3&amp;page=3&amp;include_entities=true"></a>
    <div class="row-fluid">
        <?php if (!empty($class)) {
            echo '<div class="' . $class . '">';
        }
        $img1=of_get_option('twitter1_avatar');
        $name1=of_get_option('name1');
        $screenname= of_get_option('screen_name1');
        $img1 = !empty($img1) ? $img1 : BEAU_IMAGES.'/socials/beautheme.jpg';
        $name1 = !empty($name1) ? $name1 : 'Beau Theme';
        $screenname = !empty($screenname) ? $screenname : '@Beautheme2014';
        ?>
        <div class="span2">
            <a href="https://twitter.com/<?php echo $screenname;?>"  target="_blank"><img src="<?php echo $img1;?>" alt="<?php echo $name1;?>"></a>
            <div class="name"><?php echo $name1; ?></div>
            <div class="acc-tweet"><?php echo $screenname; ?></div>
        </div>
        <div class="span10 tweet-text">
            <?php
            beau_tweets_list(
                ''.of_get_option('consumer_key1').'',
                ''.of_get_option('consumer_secret1').'',
                ''.of_get_option('access_token1').'',
                ''.of_get_option('access_secret1').'',
                ''.of_get_option('screen_name1').'',
                ''.of_get_option('num_display1').'');
            ?>
        </div>
        <?php
        if(empty($class)) {
            echo '</div><div class="row-fluid">';
        } else {
            echo '</div>';
            echo '<div class="' . $class . '">';
        }
        $img2=of_get_option('twitter2_avatar');
        $name2=of_get_option('name2');
        $screenname2= of_get_option('screen_name2');
        $img2 = !empty($img2) ? $img2 : BEAU_IMAGES.'/socials/beautheme.jpg';
        $name2 = !empty($name2) ? $name2 : 'Beau Theme';
        $screenname2 = !empty($screenname2) ? $screenname2 : '@Beautheme2014';
        ?>
        <div class="span2">
            <a href="https://twitter.com/<?php echo $screenname;?>" target="_blank"><img src="<?php echo $img2;?>" alt="<?php echo $name2;?>"></a>
            <div class="name"><?php echo $name2; ?></div>
            <div class="acc-tweet"><?php echo $screenname2; ?></div>
        </div>
        <div class="span10 tweet-text">
            <?php
            beau_tweets_list(
                ''.of_get_option('consumer_key2').'',
                ''.of_get_option('consumer_secret2').'',
                ''.of_get_option('access_token2').'',
                ''.of_get_option('access_secret2').'',
                ''.of_get_option('screen_name2').'',
                ''.of_get_option('num_display2').'');
            ?>        </div>
        <?php if(!empty($class)) {
            echo '</div>';
        }?>
    </div>
    <?php if(!empty($author)){?>
    <div class="question">
        <div class="question-bg">
            <div class="content">
                <blockquote>
                    <div><?php echo get_post_meta(get_the_ID(), '_page_tweet_question', TRUE); ?><span class="at"></span></div>
                </blockquote>
            </div>
            <div class="author">~ <?php echo get_post_meta(get_the_ID(), '_page_tweet_author', TRUE); ?> ~</div>
        </div>
        <div class="question-border"></div>
    </div>
<?php }?>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}
add_shortcode('last_tweets', 'beau_tweets');


function beau_tweets_style2($atts, $content = null, $code = '')
{
    extract(shortcode_atts(array(
        'class' => '',
    ), $atts));
    ?>
    <h2><?php echo get_post_meta(get_the_ID(), '_page_tweet_text', TRUE); ?></h2>
    <div class="row-fluid">
        <div class="span6  beau-animated move-to-right">
            <div class="span9 tweet-text">
                <?php
                beau_tweets_list(
                    ''.of_get_option('consumer_key1').'',
                    ''.of_get_option('consumer_secret1').'',
                    ''.of_get_option('access_token1').'',
                    ''.of_get_option('access_secret1').'',
                    ''.of_get_option('screen_name1').'',
                    ''.of_get_option('num_display1').'');
                ?>
            </div>
            <div class="span3">
                <?php
                $img1=of_get_option('twitter1_avatar');
                $name1=of_get_option('name1');
                $screenname= of_get_option('screen_name1');
                $img1 = !empty($img1) ? $img1 : BEAU_IMAGES.'/socials/beautheme.jpg';
                $name1 = !empty($name1) ? $name1 : 'Beau Theme';
                $screenname = !empty($screenname) ? $screenname : '@Beautheme2014';
                $img_alt1=wp_get_attachment(get_attachment_id_from_src($img1));
                ?>
                <a href="https://twitter.com/<?php echo $screenname;?>"  target="_blank"><img src="<?php echo $img1; ?>" alt="<?php echo $img_alt1['alt'];?>"></a>
                <div class="name"><?php echo $name1; ?></div>
                <div class="acc-tweet"><?php echo $screenname; ?></div>
            </div>
        </div>
        <div class="span6  beau-animated move-to-left">
            <div class="span3">
                <?php
                $img2=of_get_option('twitter2_avatar');
                $name2=of_get_option('name2');
                $screenname2= of_get_option('screen_name2');
                $img2 = !empty($img2) ? $img2 : BEAU_IMAGES.'/socials/beautheme.jpg';
                $name2 = !empty($name2) ? $name2 : 'Beau Theme';
                $screenname2 = !empty($screenname2) ? $screenname2 : '@Beautheme2014';
                $img_alt2=wp_get_attachment(get_attachment_id_from_src($img2));
                ?>
                <a href="https://twitter.com/<?php echo $screenname;?>" target="_blank"><img src="<?php echo $img2; ?>" alt="<?php echo $img2['alt'];?>"></a>
                <div class="name"><?php echo $name2; ?></div>
                <div class="acc-tweet"><?php echo $screenname2; ?></div>
            </div>
            <div class="span9 tweet-text" style="text-align: left;">
                <?php
                beau_tweets_list(
                    ''.of_get_option('consumer_key2').'',
                    ''.of_get_option('consumer_secret2').'',
                    ''.of_get_option('access_token2').'',
                    ''.of_get_option('access_secret2').'',
                    ''.of_get_option('screen_name2').'',
                    ''.of_get_option('num_display2').'');
                ?>
            </div>
        </div>
    </div>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('tweets_style2', 'beau_tweets_style2');
/*
 * Shortcode Event
 * */
function beau_event($atts, $content = null, $code = '')
{
    global $imgsize;
    extract(shortcode_atts(array(
        'post_not_in' => '',
        'item_number' => '4'
    ), $atts));
    $args = array(
        'post_type' => 'event',
        'posts_per_page' => $item_number,
    );
    $loop = new WP_Query($args);
    ?>
    <h2><?php echo of_get_option('event_header'); ?><?php echo get_post_meta(get_the_ID(), '_page_event_text', TRUE); ?></h2>
    <div class="bg-flower"><img src="<?php echo BEAU_IMAGES; ?>/icons/bg-home-event.png" alt="Event <?php the_title();?>"></div>
    <div class="home-event-text"><?php echo get_post_meta(get_the_ID(), '_page_event_des', TRUE); ?></div>
    <div class="bg-icon-cut"></div>
    <?php if ($loop->have_posts()) : ?>
    <div id="evet-slide" class="">
        <div class="next"></div>
        <div class="jcarouse">
            <ul>
                <?php while ($loop->have_posts()) : $loop->the_post();
                    $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    $location = get_post_meta(get_the_ID(), '_event_location', TRUE);
                    $time_start = get_post_meta(get_the_ID(), '_event_time_start', TRUE);
                    $time_end = get_post_meta(get_the_ID(), '_event_time_end', TRUE);
                    ?>
                    <li>
                        <div class="info span5">
                            <a href="<?php echo get_permalink() ?>" class="title"><?php the_title(); ?></a>

                            <div class="time"><?php echo $time_start . ' - ' . $time_end ?></div>
                            <div class="location">location:<br><?php echo $location; ?></div>
                            <div class="details"><?php beau_excerpt(8, true); ?></div>
                        </div>
                        <div class="thumb span7 hover-img">
                            <a href="<?php echo get_permalink() ?>">
                                <?php beau_resizer($featuredID[0], $imgsize['event']['w'], $imgsize['event']['h'], false, ''); ?>
                                <div class="image-op"></div>
                                <figure class="effect-bubba">
                                    <figcaption>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                        <div class="clr"></div>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
        <div class="prev"></div>
    </div>
    <?php
    wp_reset_postdata();
endif;
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('event', 'beau_event');

function beau_event_style2($atts, $content = null, $code = '')
{
    global $imgsize;
    extract(shortcode_atts(array(
        'post_not_in' => '',
        'item_number' => '4'
    ), $atts));
    $args = array(
        'post_type' => 'event',
        'posts_per_page' => 3,
    );
    $loop = new WP_Query($args);
    ?>
    <h2><?php echo of_get_option('event_header'); ?><?php echo get_post_meta(get_the_ID(), '_page_event_text', TRUE); ?></h2>
    <?php if ($loop->have_posts()) : ?>
    <div id="evet-slide">
        <div class="flexslider flex-event">
            <ul class="slides">
                <?php while ($loop->have_posts()) : $loop->the_post();
                    $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    $location = get_post_meta(get_the_ID(), '_event_location', TRUE);
                    $time_start = get_post_meta(get_the_ID(), '_event_time_start', TRUE);
                    $time_end = get_post_meta(get_the_ID(), '_event_time_end', TRUE);
                    $img_alt=wp_get_attachment(get_attachment_id_from_src($featuredID[0]));
                    ?>
                    <li>
                        <img src="<?php echo $featuredID[0]; ?>" alt="<?php echo $img_alt['alt'];?>"/>
                    </li>
                <?php
                endwhile;
                ?>
            </ul>
            <ol class="flex-control-nav">
                <?php while ($loop->have_posts()) : $loop->the_post();
                    $location = get_post_meta(get_the_ID(), '_event_location', TRUE);
                    $time_start = get_post_meta(get_the_ID(), '_event_time_start', TRUE);
                    $time_end = get_post_meta(get_the_ID(), '_event_time_end', TRUE);
                    ?>
                    <li>
                        <a href="<?php echo get_permalink() ?>" class="title"><?php the_title(); ?></a>
                        <div class="time"><?php echo $time_start . ' - ' . $time_end ?></div>
                        <div class="location">location:<br><?php echo $location; ?></div>
                    </li>
                <?php
                endwhile;
                ?>
            </ol>
        </div>
    </div>
    <?php
    wp_reset_postdata();
endif;
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('event_style2', 'beau_event_style2');

function beau_album($atts, $content = null, $code = '')
{
    global $imgsize;
    extract(shortcode_atts(array(
        'post_not_in' => '',
        'item_number' => '4'
    ), $atts));
    $args = array(
        'post_type' => 'gallery',
        'posts_per_page' => 99,
        'text' => ''
    );
    $loop = new WP_Query($args);
    ?>
    <section>
        <div class="beau-gallery-slide">
            <h2 class="gallery-slide-title"><?php echo get_post_meta(get_the_ID(), '_page_album_text', TRUE); ?></h2>
            <div class="gallery-slide-content"><?php echo get_post_meta(get_the_ID(), '_page_album_des', TRUE); ?></div>
        </div>
        <?php if ($loop->have_posts()) : ?>
            <div class="gallery-slide">
                <section id="photostack" class="photostack">
                    <div>
                        <?php while ($loop->have_posts()) : $loop->the_post();
                            $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                            ?>
                            <figure>
                                <a href="#"
                                   class="photostack-img"><?php beau_resizer($featuredID[0], $imgsize['album']['w'], $imgsize['album']['h'], true, '') ?></a>
                                <figcaption>
                                    <h2 class="photostack-title"><?php the_title(); ?></h2>
                                    <div class="photostack-back">
                                        <?php the_content(); ?>
                                    </div>
                                </figcaption>
                            </figure>
                        <?php endwhile; ?>
                    </div>
                    <div class="gallery-showdow"></div>
                </section>
            </div>
            <?php
            wp_reset_postdata();
        endif; ?>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('album', 'beau_album');
function beau_album_style2($atts, $content = null, $code = '')
{
    global $imgsize;
    extract(shortcode_atts(array(
        'post_not_in' => '',
        'item_number' => '4'
    ), $atts));
    $args = array(
        'post_type' => 'gallery',
        'posts_per_page' => 8,
        'text' => ''
    );
    $loop = new WP_Query($args);
    ?>
    <section>
        <div class="beau-gallery-slide style2">
            <h2 class="gallery-slide-title"><?php echo get_post_meta(get_the_ID(), '_page_album_text', TRUE); ?></h2>
        </div>
        <?php if ($loop->have_posts()) : ?>
        <div class="gallery style2">
            <?php
            $categories = get_categories(array('taxonomy' => 'gallery-category'));
            $datatype = array();
            foreach ($categories as $category) {
                $datatype[] = $category->slug;
            }
            $datatype = implode(' ', $datatype);
            ?>
            <div class="row-fluid">
                <div class="span12 gallery-category-style2">
                    <div class="gallery-category-container-style2">
                        <div class="gallery-menu style2">
                            <ul id="filters" class="inline">
                                <li class="active"><a href="#" data-filter="*"><?php _e('All', 'beauthemes'); ?></a>
                                </li>
                                <?php
                                $categories = get_categories(array('taxonomy' => 'gallery-category'));
                                foreach ($categories as $category) {?>
                                    <li><a href="#" data-filter=".<?php echo $category->slug; ?>"><?php echo $category->cat_name; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid" id="ourHolder">
                <?php
                global $imgsize;
                $args = array(
                    'post_type' => 'gallery',
                    'posts_per_page' => 8,
                );
                $loop = new WP_Query($args);
                ob_start();
                if ($loop->have_posts()) :    while ($loop->have_posts()) : $loop->the_post();
                    $terms = get_the_terms(get_the_ID(), 'gallery-category');
                    $datatype = array();
                    if(!empty($terms)){
                        foreach ( $terms as $term ) {
                            $datatype[] = $term->slug;
                        }
                    }
                    $datatype = implode(' ', $datatype);
                    $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    ?>
                    <div id="post-<?php the_ID(); ?>" class="span3 grid cs-style-7 clearfix <?php echo $datatype ?>">
                        <a class="zoom" href="<?php echo $featuredID[0]; ?>" data-rel="prettyPhoto">
                            <?php echo beau_resizer($featuredID[0], $imgsize['gallery']['w'], $imgsize['gallery']['h'], true, get_the_title()); ?>
                            <div class="zoom-overlay"><div class="title"><?php the_title(); ?></div></div>
                            <?php the_content(); ?>
                        </a>
                    </div>
                <?php
                endwhile;
                endif; ?>
            </div>
            </div>
            <?php
            wp_reset_postdata();
            endif;
            ?>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('album_style2', 'beau_album_style2');
function beau_blog($atts, $content = null, $code = '')
{
    global $imgsize;
    extract(shortcode_atts(array(
        'post_not_in' => '',
        'item_number' => '4'
    ), $atts));
    $loop = new WP_Query('&posts_per_page=' . $item_number);
    ?>
    <section class="beau-blog stretch-layout">
        <div class="blog-title"><?php echo get_post_meta(get_the_ID(), '_page_blog_text', TRUE); ?></div>
        <div class="blog-content"><?php echo get_post_meta(get_the_ID(), '_page_blog_des', TRUE); ?></div>
        <?php if ($loop->have_posts()) : ?>
            <div class="blog-lists">
                <?php $i = 0;
                while ($loop->have_posts()) : $loop->the_post();
                    $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    $date = strtotime( get_the_date() );
                    if ($i % 2 != 0) {
                        ?>
                        <div class="row-fluid">
                            <div class="span6 blog-odd hover-img beau-animated move-to-right">
                                <a href="<?php echo get_permalink() ?>">
                                    <?php beau_resizer($featuredID[0], $imgsize['blog']['w'], $imgsize['blog']['h'], true, get_the_title(get_the_ID())); ?>
                                    <div class="image-op"></div>
                                    <div class="odd">
                                        <figure class="effect-bubba">
                                            <figcaption>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </a>
                            </div>
                            <div class="span6 blog-odd-r beau-animated move-to-left">
                                <div class="odd-bg"></div>
                                <div class="content">
                                    <a href="<?php echo get_permalink() ?>">
                                        <h3><?php the_title(); ?></h3></a>

                                    <div class="date"><?php echo date( 'l', $date ).' '.date( 'j', $date ).' '.date( 'F ', $date ).' '.date( 'Y ', $date ); ?></div>
                                    <div class="blog-description"><?php echo beau_excerpt(54); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php
                    } else {
                        ?>
                        <div class="row-fluid bg-r">
                            <div class="span6 blog-even-l beau-animated move-to-right">
                                <div class="odd-bg"></div>
                                <div class="content">
                                    <a href="<?php echo get_permalink() ?>"><h3><?php the_title(); ?></h3></a>
                                    <div class="date"><?php echo date( 'l', $date ).' '.date( 'j', $date ).' '.date( 'F ', $date ).' '.date( 'Y ', $date ); ?></div>
                                    <div class="blog-description"><?php echo beau_excerpt(54); ?></div>
                                </div>
                            </div>
                            <div class="span6 blog-odd hover-img beau-animated move-to-left">
                                <a href="<?php echo get_permalink() ?>">
                                    <?php beau_resizer($featuredID[0], $imgsize['blog']['w'], $imgsize['blog']['h'], true, get_the_title(get_the_ID())); ?>
                                    <div class="image-op"></div>
                                    <div class="odd">
                                        <figure class="effect-bubba">
                                            <figcaption>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    <?php $i++; endwhile; ?>
            </div>
            <?php
            wp_reset_postdata();
        endif;
        ?>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('blog', 'beau_blog');
function beau_blog_style2($atts, $content = null, $code = '')
{
    global $imgsize;
    extract(shortcode_atts(array(
        'post_not_in' => '',
        'item_number' => '4'
    ), $atts));
    $loop = new WP_Query('&posts_per_page=' . $item_number);
    ?>
    <section class="beau-blog stretch-layout style2">
        <div class="blog-title"><?php echo get_post_meta(get_the_ID(), '_page_blog_text', TRUE); ?></div>
        <?php if ($loop->have_posts()) : ?>
            <div class="blog-lists blog-lists-full-layout">
                <?php $i = 0;
                while ($loop->have_posts()) : $loop->the_post();
                    $featuredID = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');?>
                    <div class="row-fluid">
                        <div class="blog-list-container">
                            <div class="span12 beau-animated fade-in">
                                <div class="full-content">
                                    <div class="row-fluid">
                                        <div class="span1">
                                            <div class="date"><span><?php echo get_the_time('d') ?></span></div>
                                            <div class="month-year">
                                                <span><?php echo substr(get_the_time('F'), 0, 3); ?></span></div>
                                        </div>
                                        <div class="span11">
                                            <a href="<?php echo get_permalink() ?>"><h3><?php the_title(); ?></h3></a>

                                            <div class="content style2">
                                                <?php beau_excerpt(30, true); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <a href="<?php echo get_permalink() ?>">
                                            <div class="post-thumb hover-img">
                                                <!--                                                <img src="--><?php //echo $featuredID[0] ?><!--"-->
                                                <!--                                                     alt="--><?php //echo get_the_title(); ?><!--">-->
                                                <?php beau_resizer($featuredID[0], $imgsize['blog2']['w'], $imgsize['blog2']['h'], true, get_the_title()); ?>
                                                <div class="image-op"></div>
                                                <div class="odd">
                                                    <figure class="effect-bubba">
                                                        <figcaption>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;  endwhile;
                ?>
            </div>
            <?php
            wp_reset_postdata();
        endif;
        ?>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('blog_style2', 'beau_blog_style2');

function beau_attendingform($atts, $content = null, $code = '')
{
    ?>
    <section class="beau-attending-form">
        <h2 class="attending-title"><?php echo get_post_meta(get_the_ID(), '_page_attending_text', TRUE); ?></h2>
        <div class="attending-content"><?php echo get_post_meta(get_the_ID(), '_page_attending_des', TRUE); ?></div>
        <div class="row-fluid">
            <?php $rand = uniqid();
            ob_start();    ?>
            <script type="text/javascript">
                jQuery(function ($) {
                    $('#contact_<?php echo $rand; ?>').submit(function () {
                        $('#loading_contact_<?php echo $rand; ?>').fadeIn('fast');
                        $('#result_contact_<?php echo $rand; ?>').hide();
                        $.ajax({
                            type: 'POST',
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            success: function (data) {
                                $('#result_contact_<?php echo $rand; ?>').html(data);
                                $('#result_contact_<?php echo $rand; ?>').fadeIn('fast');
                                $('#loading_contact_<?php echo $rand; ?>').hide();
                            }
                        })
                        return false;
                    });
                });
            </script>
            <form id="contact_<?php echo $rand; ?>"
                  action="<?php echo BEAU_BASE_URL . '/include/scripts/attending.php'; ?>" method="post"
                  class="span12 attending">
                <!--            <form id="contact_--><?php //echo $rand; ?><!--" action="-->
                <?php //echo BEAU_BASE_URL  .'/include/scripts/contact.php'; ?><!--" method="post">-->
                <input type="text" name="fullname" placeholder="Fullname" class="span12">
                <input type="text" name="email" placeholder="Email" class="span12">
                <select name="persons_attending" class="span12">
                    <option selected="selected" value="1">Number of persons attending</option>
                    <?php for ($i = 1; $i <= 15; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }?>
                </select>
                <select name="wedding_ceremony" class="span12">
                    <option selected="selected" value="1">Wedding Ceremony</option>
                    <?php for ($i = 1; $i <= 15; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }?>
                </select>
                <input type="submit" class="btn" name="submit" value="<?php _e('I’M ATTENDING', 'beau'); ?>"
                       id="contact-submit"/>
                <input type="hidden" name="send_to" value="<?php echo $send_to; ?>"/>
                <!--                <a href="#" class="btn">I’M ATTENDING</a>-->
                <div id="result_contact_<?php echo $rand; ?>"></div>
                <div id="loading_contact_<?php echo $rand; ?>" style="margin-bottom: 20px; display: none;"></div>
            </form>
        </div>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('attendingform', 'beau_attendingform');

function beau_attendingform_style2($atts, $content = null, $code = '')
{
    ?>
    <section class="beau-attending-form style2">
        <h2 class="attending-title"><?php echo get_post_meta(get_the_ID(), '_page_attending_text', TRUE); ?></h2>
        <div class="row-fluid">
            <?php $rand = uniqid();
            ob_start();    ?>
            <script type="text/javascript">
                jQuery(function ($) {
                    $('#contact_<?php echo $rand; ?>').submit(function () {
                        $('#loading_contact_<?php echo $rand; ?>').fadeIn('fast');
                        $('#result_contact_<?php echo $rand; ?>').hide();
                        $.ajax({
                            type: 'POST',
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            success: function (data) {
                                $('#result_contact_<?php echo $rand; ?>').html(data);
                                $('#result_contact_<?php echo $rand; ?>').fadeIn('fast');
                                $('#loading_contact_<?php echo $rand; ?>').hide();
                            }
                        })
                        return false;
                    });
                });
            </script>
            <form id="contact_<?php echo $rand; ?>"
                  action="<?php echo BEAU_BASE_URL . '/include/scripts/attending.php'; ?>" method="post"
                  class="span12 attending">
                <div class="f-bg"></div>
                <div class="f-bg-left-top"></div>
                <div class="f-bg-right-top"></div>
                <div class="f-bg-left-bottom"></div>
                <div class="f-bg-right-bottom"></div>
                <input type="text" name="fullname" placeholder="Fullname" class="span12">
                <input type="text" name="email" placeholder="Email" class="span12">
                <select name="persons_attending" class="span12">
                    <option selected="selected" value="1">Number of persons attending</option>
                    <?php for ($i = 1; $i <= 15; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }?>
                </select>
                <select name="wedding_ceremony" class="span12">
                    <option selected="selected" value="1">Wedding Ceremony</option>
                    <?php for ($i = 1; $i <= 15; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }?>
                </select>
                <input type="submit" class="btn" name="submit" value="<?php _e('I’M ATTENDING', 'beau'); ?>"
                       id="contact-submit"/>
                <input type="hidden" name="send_to" value="<?php echo $send_to; ?>"/>

                <div id="result_contact_<?php echo $rand; ?>"></div>
                <div id="loading_contact_<?php echo $rand; ?>" style="margin-bottom: 20px; display: none;"></div>
            </form>
        </div>
    </section>
    <?php
    $out = ob_get_contents();
    ob_end_clean();
    return $out;
}

add_shortcode('attendingform_style2', 'beau_attendingform_style2');

function beau_button($atts, $content = null)
{
    extract(shortcode_atts(array(
        'icon' => '',
        'size' => 'small',
        'color' => '',
        'url' => '#'
    ), $atts));

    $attstoclass = '';
    if (!empty($icon)) $attstoclass = '<i class="' . $icon . '"></i>';

    $out = '<a class="st-button ' . $size . ' ' . $color . '" href="' . $url . '">' . $attstoclass . strip_tags($content) . '</a>';
    return $out;
}

add_shortcode('button', 'beau_button');
/*******************************************************************
Formatter
 ********************************************************************/
function beau_do_shortcode($content, $autop = FALSE)
{
    $content = do_shortcode($content);

    if ($autop) {
        $content = wpautop($content);
    }

    return $content;
}

function beau_fix_shortcodes($content)
{
    $array = array(
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );
    $content = strtr($content, $array);
    return $content;
}
add_filter('the_content', 'beau_fix_shortcodes');
add_filter('widget_text', 'beau_fix_shortcodes');
add_filter('widget_text', 'do_shortcode');