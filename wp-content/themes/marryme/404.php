<?php get_header(); ?>
<!-- main-container -->
<div id="main-wrapper" role="main">
	<div class="container">
	<div class="row-fluid">
		<div id="content-wrapper" class="span12 content-full">
			<!-- post entry -->
			<article class="page-single clearfix page-not-found">
				<div class="post-content text-center">
					<div class="text404">
                        <?php echo '<h2>404</h2>'?>
                        <div><img src="<?php echo BEAU_IMAGES.'/icons/bg-getting-married.png'?>" alt="<?php the_title();?>"></div>
                        <div class="text-des">Sorry! that page doesn&#39;t seem to exist.</div>
                    </div>
				</div>
			</article>
			<!-- end post entry -->
		</div>
	</div>
	</div>
</div>
<!-- end main-container -->
<?php get_footer(); ?>