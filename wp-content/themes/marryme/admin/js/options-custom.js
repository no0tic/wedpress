/**
 * Custom scripts needed for the colorpicker, image button selectors,
 * and navigation tabs.
 */

jQuery(document).ready(function($) {

	/* Loads the color pickers*/
	$('.of-color').wpColorPicker();

	/* Image Options*/
	$('.of-radio-img-img').click(function(){
		$(this).parent().parent().find('.of-radio-img-img').removeClass('of-radio-img-selected');
		$(this).addClass('of-radio-img-selected');
	});

	$('.of-radio-img-label').hide();
	$('.of-radio-img-img').show();
	$('.of-radio-img-radio').hide();

	/* Loads tabbed sections if they exist*/
	if ( $('.nav-tab-wrapper').length > 0 ) {
		options_framework_tabs();
	}

	function options_framework_tabs() {

		var $group = $('.group'),
			$navtabs = $('.nav-tab-wrapper a'),
			active_tab = '';

		/*// Hides all the .group sections to start*/
		$group.hide();

		/*// Find if a selected tab is saved in localStorage*/
		if ( typeof(localStorage) != 'undefined' ) {
			active_tab = localStorage.getItem('active_tab');
		}

		/*// If active tab is saved and exists, load it's .group*/
		if ( active_tab != '' && $(active_tab).length ) {
			$(active_tab).fadeIn();
			$(active_tab + '-tab').addClass('nav-tab-active');
		} else {
			$('.group:first').fadeIn();
			$('.nav-tab-wrapper a:first').addClass('nav-tab-active');
		}

		/*// Bind tabs clicks*/
		$navtabs.click(function(e) {

			e.preventDefault();

			/*// Remove active class from all tabs*/
			$navtabs.removeClass('nav-tab-active');

			$(this).addClass('nav-tab-active').blur();

			if (typeof(localStorage) != 'undefined' ) {
				localStorage.setItem('active_tab', $(this).attr('href') );
			}

			var selected = $(this).attr('href');

			$group.hide();
			$(selected).fadeIn();

		});
	}

	/* if ($('#enable-map').is(":checked")) {
	 	$('#section-map-longitude, #section-map-latitude, #section-map-zoom').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-facebook').is(":checked")) {
	 	$('#facebook').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-twitter').is(":checked")) {
	 	$('#twitter').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-youtube').is(":checked")) {
	 	$('#youtube').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-google').is(":checked")) {
	 	$('#google').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-linkedin').is(":checked")) {
	 	$('#linkedin').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-pinterest').is(":checked")) {
	 	$('#pinterest').fadeIn('slow');
	 }
	 if ($('#options_theme-show-socials-rss').is(":checked")) {
	 	$('#rss').fadeIn('slow');
	 }

	$('#facebook, #twitter, #google, #youtube, #linkedin, #rss, #section-map-longitude, #section-map-latitude, #section-map-zoom').hide();*/
	$('#enable-map').click(function() {
		if ($('#enable-map').is(":checked")) {
			$('#section-map-longitude, #section-map-latitude, #section-map-zoom').fadeIn('slow');
		}else{
			$('#section-map-longitude, #section-map-latitude, #section-map-zoom').hide('slow');
			$('#map-longitude, #map-latitude, #map-zoom').attr('value', '');
		}
	});
$('select#font_type').each(function(){
    var heading_option = $(this).children('option:selected').val();
    if (heading_option == '0') {
        $('#section-google_font').hide('slow');
        $('#section-google_in_font').hide('slow');
        $('#section-demo_web_font').hide('slow');
        $('#section-demo_font').hide('slow');
    }
    else if (heading_option == '1') {
        $('#section-google_font').fadeIn('slow');
        $('#section-google_in_font').hide('slow');
        $('#section-demo_web_font').fadeIn('slow');
        $('#section-demo_font').hide('slow');
    }
    else if (heading_option == '2') {
        $('#section-google_font').hide('slow');
        $('#section-google_in_font').fadeIn('slow');
        $('#section-demo_web_font').hide('slow');
        $('#section-demo_font').fadeIn('slow');
    }
}).change(function(){
        var heading_option = $(this).children('option:selected').val();
        if (heading_option == '0') {
            $('#section-google_font').hide('slow');
            $('#section-google_in_font').hide('slow');
            $('#section-demo_web_font').hide('slow');
            $('#section-demo_font').hide('slow');
        }
        else if (heading_option == '1') {
            $('#section-google_font').fadeIn('slow');
            $('#section-google_in_font').hide('slow');
            $('#section-demo_web_font').fadeIn('slow');
            $('#section-demo_font').hide('slow');
        }
        else if (heading_option == '2') {
            $('#section-google_font').hide('slow');
            $('#section-google_in_font').fadeIn('slow');
            $('#section-demo_web_font').hide('slow');
            $('#section-demo_font').fadeIn('slow');
        }
    })
$('#google_in_font').each(function(){
        var heading_option = $(this).children('option:selected').val();
        $('#section-demo_font').css('font-family',$(this).children('option:selected').val());
}).change(function(){
        var mainID = $(this).attr('id');
        $('#section-demo_font').css('font-family',''+$(this).children('option:selected').val()+'');
        GoogleFontSelect( this, mainID );
});
$('#google_font').each(function(){
    var heading_option = $(this).children('option:selected').val();
    $('#section-demo_font').css('font-family',$(this).children('option:selected').val());
}).change(function(){
        $('#section-demo_font').css('font-family',''+$(this).children('option:selected').val()+'');
});
    function GoogleFontSelect( slctr, mainID ){
        var _selected = $(slctr).val(); 						/*//get current value - selected and saved*/
        var _linkclass = 'style_link_'+ mainID;
        var _previewer = mainID +'_ggf_previewer';

        if( _selected ){ /*//if var exists and isset
*/
            $('.'+ _previewer ).fadeIn();

           /* //Check if selected is not equal with "Select a font" and execute the script.*/
            if ( _selected !== 'none' && _selected !== 'Select a font' ) {

                /*//remove other elements crested in <head>*/
                $( '.'+ _linkclass ).remove();

                /*//replace spaces with "+" sign*/
                var the_font = _selected.replace(/\s+/g, '+');

                /*//add reference to google font family*/
                $('head').append('<link href="http://fonts.googleapis.com/css?family='+ the_font +'" rel="stylesheet" type="text/css" class="'+ _linkclass +'">');

                /*//show in the preview box the font*/
                $('.'+ _previewer ).css('font-family', _selected +', sans-serif' );

            }else{

                /*//if selected is not a font remove style "font-family" at preview box*/
                $('.'+ _previewer ).css('font-family', '' );
                $('.'+ _previewer ).fadeOut();

            }

        }

    }

   /*  $('#options_theme-show-socials-facebook').click(function() {
	 	if ($('#options_theme-show-socials-facebook').is(":checked")) {
	 		$('#facebook').fadeIn('slow');
	 	}else{
	 		$('#facebook').hide('slow');
	 		$('#facebook').attr('value', '');
	 	}
	 });

	 $('#options_theme-show-socials-twitter').click(function() {
	 	if ($('#options_theme-show-socials-twitter').is(":checked")) {
	 		$('#twitter').fadeIn('slow');
	 	}else{
	 		$('#twitter').hide('slow');
	 		$('#twitter').attr('value', '');
	 	}
	 });

	 $('#options_theme-show-socials-youtube').click(function() {
	 	if ($('#options_theme-show-socials-youtube').is(":checked")) {
	 		$('#youtube').fadeIn('slow');
	 	}else{
	 		$('#youtube').hide('slow');
	 		$('#youtube').attr('value', '');
	 	}
	 });

	 $('#options_theme-show-socials-google').click(function() {
	 	if ($('#options_theme-show-socials-google').is(":checked")) {
	 		$('#google').fadeIn('slow');
	 	}else{
	 		$('#google').hide('slow');
	 		$('#google').attr('value', '');
	 	}
	 });

	 $('#options_theme-show-socials-linkedin').click(function() {
	 	if ($('#options_theme-show-socials-linkedin').is(":checked")) {
	 		$('#linkedin').fadeIn('slow');
	 	}else{
	 		$('#linkedin').hide('slow');
	 		$('#linkedin').attr('value', '');
	 	}
	 });

	 $('#options_theme-show-socials-pinterest').click(function() {
	 	if ($('#options_theme-show-socials-pinterest').is(":checked")) {
	 		$('#pinterest').fadeIn('slow');
	 	}else{
	 		$('#pinterest').hide('slow');
	 		$('#pinterest').attr('value', '');
	 	}
	 });

	 $('#options_theme-show-socials-rss').click(function() {
	 	if ($('#options_theme-show-socials-rss').is(":checked")) {
	 		$('#rss').fadeIn('slow');
	 	}else{
	 		$('#rss').hide('slow');
	 		$('#rss').attr('value', '');
	 	}
	 });
*/
});