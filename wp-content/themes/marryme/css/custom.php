<?php
$slice_url = dirname(__FILE__);
if ( preg_match('#wp-content#isU', $slice_url) ) {
	require_once '../../../../wp-load.php';  
}
else {
	require_once '../../../../../wp-load.php';  
}  header("Content-type: text/css");
	$style_custom_color =  of_get_option('style_custom_color');
	$style_custom_font_color_heading =  of_get_option('style_custom_font_color_heading');
	$style_font =  of_get_option('font_type');
	$style_custom_font =  of_get_option('google_font');
	$style_custom_gg_font =  of_get_option('google_in_font');
if($style_font==1 && $style_custom_font){
    echo 'body,button.btn, input[type="submit"].btn,.row-fluid input[class*="span"],.btn, select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input{
    font-family: '.$style_custom_font.', Arial, sans-serif;
    }';
}elseif($style_font==2 && $style_custom_gg_font){
    echo 'body,button.btn, input[type="submit"].btn,.row-fluid input[class*="span"],.btn, select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input{
    font-family: '.$style_custom_gg_font.', Arial, sans-serif;
    }';
}
if ( $style_custom_color ) {

		echo "a.more-link, code,
#comments .commentlist .comment .commentmetadata a,
 #footer .widget-twitter li span a,
.st-accordion .accordion-title.active,
.st-icon, .st-accordion .accordion-title:before,
.widget-twitter li:before,
.widget-twitter li span a,
.getting-married-date-count .number ul li,
#evet-slide a.title,
.home-tweet-follow .tweet-text a,
.beau-blog h3,
.beau-temp-event-list .span6 h2,
.temp-event-item-location,
.event-author .postion,
.beau-blog-detail .blog-meta span,
.beau-blog-detail .blog-meta span a,
.comment a,
.beau-temp-event-list-devoted:before,
.beau-attending-form.style2 input.btn,
.span6.contact-map span a, .map-des a,
.home-tweet-follow .name,
.gallery-menu ul.inline li.active a,
.gallery-menu ul.inline li a:hover,
.guest-says .g-name{
	color:$style_custom_color;
	transition: all 100ms linear;
}
.beau-getting-married .getting-married,
.beau-gallery-slide .gallery-slide-title,
.beau-blog .blog-title, .beau-attending-form .attending-title,
.beau-contact .contact-title,
.beau-event-tweet.beau-temp-event h1,
.beau-couple-slide .couple-name,
a:hover, a:focus,
.widget_rss li a:hover,
#footer #copyright-wrapper a:hover,
#footer a:hover,
.blog-list .post-title a:hover,
.home-tweet-follow .tweet-text a:hover,
.jcarouse ul li a:hover,
#evet-slide a.title:hover,
.beau-blog  a:hover h3,
.beau-temp-event-list .span6 a:hover h2,
.beau-event-tweet h2,
.breadcrumb .active, .breadcrumb li a:hover,
h3.widget-title,
#evet-slide .title,
.guest-says .loadmore a,
.widget-twitter li span a:hover,
.style2 #evet-slide .flex-control-nav a.title:hover,
.beau-getting-married.style2 .getting-married,
.beau-gallery-slide.style2 .gallery-slide-title,
 .beau-blog.style2 .blog-title,
 .beau-attending-form.style2 .attending-title,
 .beau-contact.style2 .contact-title,
 .beau-event-tweet.style2 h2,
.style2 .beau-event-tweet.beau-temp-event h1,
a:hover, a:focus,
.widget_rss li a:hover,
#footer #copyright-wrapper a:hover,
#footer a:hover,
.blog-list .post-title a:hover,
.beau-blog  a:hover h3,
.pagination .current,
.page-not-found .text-des,
.widget_tag_cloud a:hover
{
    	color:$style_custom_font_color_heading;
    	transition: all 100ms linear;
}
.beau-couple-slide .couple-social li:hover,.blog-social li:hover
{
    	border-color:$style_custom_color;
    	transition: all 100ms linear;
}
.btn, button, html input[type=\"button\"],
input[type=\"reset\"], input[type=\"submit\"],
.flickr_badge_image a,
.widget_recent_entries li:before,
.widget_recent_comments li:before,
.home-tweet-follow a.btn,
.beau-temp-event-list .beau-temp-event-item-l:after,
.beau-temp-event-list .beau-temp-event-item-r:after
{
	background-color:$style_custom_color;
	transition: all 100ms linear;
}
.home-tweet-follow a.btn:hover,
.btn:hover, button:hover, html input[type=\"button\"]:hover,
input[type=\"reset\"]:hover, input[type=\"submit\"]:hover,
.beau-event-tweet.style2 .home-tweet-follow a.btn:hover,
.beau-attending-form.style2 input.btn:hover,
.beau-attending-form.style2 input.btn:hover
{
	background-color:$style_custom_font_color_heading;
	transition: all 100ms linear;
}
.home-tweet-follow a.btn,.home-tweet-follow a.btn:hover{
    color:#fff;
    transition: all 100ms linear;
}
.widget_tag_cloud a:hover,
ul.primary-menu ul{
/*border-top-color:$style_custom_color; */
}\n";
	}	

?>