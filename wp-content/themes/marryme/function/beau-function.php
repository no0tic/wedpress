<?php
/*
 * This is an example of how to add custom scripts to the options panel.
 * This one shows/hides the an option when a checkbox is clicked.
 *
 * You can delete it if you not using that option
 */

$imgsize = array();
$imgsize['slider']		= array('w' => 1400,	'h' => 850);
$imgsize['event']		= array('w' => 298,	'h' => 184);
$imgsize['event-list']		= array('w' => 544,	'h' => 322);
$imgsize['album']		= array('w' => 240,	'h' => 240);
$imgsize['blog']		= array('w' => 585,	'h' => 344);
$imgsize['blog2']		= array('w' => 770,	'h' => 255);
$imgsize['gallery']		= array('w' => 234,	'h' => 253);
$imgsize['gallery2']		= array('w' => 270,	'h' => 240);
$imgsize['couple']		= array('w' => 500,	'h' => 444);
if ( !isset($content_width) ) {
    $content_width = 960;
}

add_action('after_setup_theme', 'crucial_setup');

function crucial_setup(){
    load_theme_textdomain('marryme', get_template_directory() . '/lang');
}
if ( function_exists('add_theme_support') ) {
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'nav_menus' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-background' );
}
add_editor_style();
/*******************************************************************
Register Menu
 ********************************************************************/
if (function_exists("register_sidebar")) {
    $sidebar1 = array(
        'General',
        'Sidebar blog',
        'Footer Column 1',
        'Footer Column 2',
        'Footer Column 3',
        'Footer Column 4',
    );
    $sidebars = array_merge( (array)$sidebar1);
    $sidebars = array_filter($sidebars);
    foreach ($sidebars as $sidebar)
    {
        register_sidebar(array(
            'name' => $sidebar,
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title"><span>',
            'after_title' => '</span></h3>',
        ));
    }
}
if ( function_exists('register_nav_menus') )
{
    register_nav_menus(
        array(
            'mobile-primary' => __(THEMENAME . ' Mobile Primary', 'beautheme'),
            'left-primary' => __(THEMENAME . ' Left Primary', 'beautheme'),
            'right-primary' => __(THEMENAME . ' Right Primary Menu', 'beautheme'),
            'secondary' => __(THEMENAME . ' Secondary Menu', 'beautheme'),
        )
    );
}
add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('#example_showhidden').click(function() {
                jQuery('#section-example_text_hidden').fadeToggle(400);
            });
            if (jQuery('#example_showhidden:checked').val() !== undefined) {
                jQuery('#section-example_text_hidden').show();
            }
        });
    </script>
<?php
}
function wd_styles()
{
    global $beau_options, $theme_version;
    wp_enqueue_style('font', 'http://fonts.googleapis.com/css?family=Roboto Slab:100,300,400,700');
    wp_enqueue_style('font', 'http://fonts.googleapis.com/css?family=Raleway:400,300,600,700,500,800');
    wp_enqueue_style('prettyPhoto', BEAU_JS .'/prettyPhoto/prettyPhoto.css', array(), '3.1.5');
    wp_enqueue_style('flexslider', BEAU_JS .'/flexslider/flexslider.css', array(), '2.1');
    wp_enqueue_style('gallery-default', BEAU_CSS .'/gallery/default.css', array(), $theme_version);
    wp_enqueue_style('gallery-component', BEAU_CSS .'/gallery/component.css', array(), $theme_version);
    wp_enqueue_style('photostack', BEAU_CSS .'/photostack.css', array(), $theme_version);
    wp_enqueue_style('photostacks', BEAU_CSS .'/photostacks.css', array(), $theme_version);
    wp_enqueue_style('bootstrap', BEAU_CSS .'/bootstrap.min.css', array(), '2.3.1');
    wp_enqueue_style('layout', BEAU_CSS .'/layout.css', array(), $theme_version);
    wp_enqueue_style('bootstrap-responsive', BEAU_CSS .'/bootstrap-responsive.min.css', array(), '2.3.1');
    wp_enqueue_style('responsive', BEAU_CSS .'/responsive.css', array(), $theme_version);
    wp_enqueue_style('custom', BEAU_CSS .'/custom.php', array(), $theme_version);
    $style__ft = of_get_option('font_type');
    if( $style__ft == 1 ) {
        $style_custom_font =  of_get_option('google_font');
        wp_enqueue_style('heading-font', 'http://fonts.googleapis.com/css?family='. $style_custom_font);
    }elseif( $style__ft == 2){
        $style_custom_font =  of_get_option('google_in_font');
        wp_enqueue_style('heading-font', 'http://fonts.googleapis.com/css?family='. $style_custom_font);
    }
}
if ( !is_admin() ) {
    add_action('wp_print_styles', 'wd_styles');
    add_action('wp_print_scripts', 'wd_scripts');
}
function wd_scripts()
{
    global $beau_options;
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-bootstrap', BEAU_JS .'/bootstrap.min.js', 'jquery', '2.3.1', TRUE);
    wp_enqueue_script('jquery-easing', BEAU_JS .'/jquery.easing.js', 'jquery', '1.3', TRUE);
    wp_enqueue_script('jquery-fitvids', BEAU_JS .'/jquery.fitvids.js', 'jquery', '1.0', TRUE);
    wp_enqueue_script('jquery-viewport', BEAU_JS .'/jquery.viewport.mini.js', 'jquery', '1.0', TRUE);
    wp_enqueue_script('jquery-isotope', BEAU_JS .'/jquery.isotope.min.js', 'jquery', '1.5.25', TRUE);
    wp_enqueue_script('jquery-prettyPhoto', BEAU_JS .'/prettyPhoto/jquery.prettyPhoto.js', 'jquery', '3.1.5', TRUE);

    wp_enqueue_script('jquery-flexslider', BEAU_JS .'/flexslider/jquery.flexslider-min.js', 'jquery', '2.1', TRUE);
    wp_enqueue_script('jquery-froogaloop', BEAU_JS .'/flexslider/froogaloop.js', 'jquery', '1.0', TRUE);
    wp_enqueue_script('jquery-trantision', BEAU_JS .'/trantision.js', 'jquery', '0.9', TRUE);
    wp_enqueue_script('jquery-jcarousellit', BEAU_JS .'/jcarousellite_1.0.1c4.js', 'jquery', '1.0.1', TRUE);
    wp_enqueue_script('jquery-countdown', BEAU_JS .'/jquery.countdown.js', 'jquery', '2.1', TRUE);

    wp_register_script('jquery-gmap-api', 'http://maps.google.com/maps/api/js?sensor=false', array(), FALSE, TRUE);
    wp_register_script('jquery-gmap', BEAU_JS .'/jquery.gmap.min.js', 'jquery', '3.3.3', TRUE);

    if ( is_singular() && comments_open() && get_option('thread_comments') ) {
        wp_enqueue_script( 'comment-reply' );
    }
    wp_enqueue_script('custom-script', BEAU_JS .'/scripts.js', 'jquery', '1.0', TRUE);
    if ( is_front_page()){
        wp_enqueue_script('jquery-fluidvids', BEAU_JS .'/fluidvids.js', 'jquery', '2.1', TRUE);
    }
    $site_style= of_get_option('blog_style');
    $site_style = !empty($site_style) ? $site_style : 1;
    if($site_style==1){
        wp_enqueue_script('jquery-modernizr', BEAU_JS .'/photostack/modernizr.min.js', 'jquery', '2.1', TRUE);
        wp_enqueue_script('jquery-classie', BEAU_JS .'/photostack/classie.js', 'jquery', '2.1', TRUE);
            wp_enqueue_script('jquery-photostack', BEAU_JS .'/photostack/photostack.js', 'jquery', '2.1', TRUE);
    }
}
function beau_options($key, $default = '', $echo = FALSE)
{
    global $beau_options;

    if ( is_array($key) ) {
        foreach ($key as $val) {
            $option[$val] = stripslashes_deep($beau_options[$val]);
        }
        return $option;
    }

    $option = stripslashes_deep( of_get_option($key, $default));

    if ( empty($option) ) $option = $default;

    if ( $echo ) echo $option;

    return $option;
}

function beau_socials()
{
    $args = array('facebook', 'twitter', 'google', 'linkedin', 'pinterest');

    $socials = beau_options($args);

    $out = '[social]';
    foreach ($socials as $key => $val) {
        $name = str_replace('soc_', '', $key);
        if ( !empty($val) )
            $out.= '[item type="'. $name .'" url="'. $val .'"]'."\n";
    }
    $out.= '[item type="RSS" url="'. beau_options('soc_rss', get_bloginfo('rss2_url')) .'"]'."\n";
    $out.= '[/social]';

    echo do_shortcode($out);
}
function beau_resizer($url = '', $width = 150, $height = 150, $cropping = true, $title = '')
{
    if ( empty($url) ) return;
    if ( strstr( $url, site_url() ) ) {
        $image_url = aq_resize( $url, $width, $height, $cropping );
    } else {
        $path = pathinfo($url);
        $new_url = '-'.$width.'x'.$height.'.'.$path['extension'];
        $image_url = str_replace('.'.$path['extension'], $new_url, $url);
        if ( !@getimagesize($image_url) ) $image_url = $url;
    }
    $out = '<img src="'. $image_url .'" alt="'. $title .'" />';
    echo $out;
}
function beau_breadcrumb()
{
    global $style;
    $style=of_get_option('blog_style');
    $class='';
    $cl='';
    if($style==1){$class='style1';}elseif($style==2){$class='style2';}
    $delimiter = '<span class="divider">/</span>';
    $currentBefore = '<li class="active">';
    $currentAfter = '</li>';
    if(is_front_page()){
        $cl="invisible";
    }
    echo '<ul class="breadcrumb '.$class.' '.$cl.'">';
    global $post;
    $home = home_url();
    echo '<li><a href="'. $home .'">Home</a> '. $delimiter .'</li>';
    if ( is_tax() ) {
        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        echo $currentBefore . $term->name . $currentAfter;
    } elseif ( is_category() ) {
        global $wp_query;
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0) echo '<li>'. get_category_parents($parentCat, TRUE, ' '. $delimiter .'</li><li>') .'</li>';
        echo $currentBefore;
        single_cat_title();
        echo $currentAfter;
    } elseif ( is_day() ) {
        echo '<li><a href="'. get_year_link(get_the_time('Y')) .'">' . get_the_time('Y') . '</a> '. $delimiter .'</li>';
        echo '<li><a href="'. get_month_link(get_the_time('Y'),get_the_time('m')) .'">' . get_the_time('F') . '</a> '. $delimiter .'</li>';
        echo $currentBefore . get_the_time('d') . $currentAfter;
    } elseif ( is_month() ) {
        echo '<li><a href="'. get_year_link(get_the_time('Y')) .'">' . get_the_time('Y') . '</a> '. $delimiter .'</li>';
        echo $currentBefore . get_the_time('F') . $currentAfter;

    } elseif ( is_year() ) {
        echo $currentBefore . get_the_time('Y') . $currentAfter;
    } elseif ( is_single() ) {
        $postType = get_post_type();
        if ( $postType == 'post' ) {
            echo '<li>Blog'. $delimiter .'</li>';
            echo $currentBefore;
            echo get_the_title(get_the_ID());
            echo $currentAfter;
        }
        elseif($postType == 'event')
        {
            echo '<li>Event'. $delimiter .'</li>';
            echo $currentBefore;
            echo get_the_title(get_the_ID());
            echo $currentAfter;
        }
    } elseif ( is_page() && !$post->post_parent ) {
        echo $currentBefore;
        the_title();
        echo $currentAfter;

    } elseif ( is_page() && $post->post_parent ) {
        $parent_id  = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="'. get_permalink($page->ID) .'">' . get_the_title($page->ID) . '</a> '. $delimiter .'</li></li>';
            $parent_id  = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb) echo '<li>'. $crumb .'</li>';
        echo $currentBefore;
        the_title();
        echo $currentAfter;
    } elseif ( is_search() ) {
        echo $currentBefore . __('Search Results for:', 'slicetheme'). ' &quot;' . get_search_query() . '&quot;' . $currentAfter;
    } elseif ( is_tag() ) {
        echo $currentBefore . __('Post Tagged with:', 'slicetheme'). ' &quot;';
        single_tag_title();
        echo '&quot;' . $currentAfter;
    } elseif ( is_author() ) {
        global $author;
        $userdata = get_userdata($author);
        echo $currentBefore . __('Author Archive', 'slicetheme') . $currentAfter;

    } elseif ( is_404() ) {
        echo $currentBefore . __('Page Not Found', 'slicetheme') . $currentAfter;

    }
    echo '</ul>';
}

function beau_excerpt($length = 35, $echo = FALSE)
{
    global $post;
    $the_excerpt = strip_shortcodes( $post->post_content );
    $the_excerpt = str_replace(']]>', ']]&gt;', $the_excerpt);
    $the_excerpt = strip_tags($the_excerpt);
    $words = preg_split("/[\n\r\t ]+/", $the_excerpt, $length + 1, PREG_SPLIT_NO_EMPTY);
    if ( count($words) > $length ) {
        array_pop($words);
        $the_excerpt = implode(' ', $words);
        $the_excerpt = $the_excerpt;
    } else {
        $the_excerpt = implode(' ', $words);
    }

    $the_excerpt = wpautop($the_excerpt);

    if ( $echo ) echo $the_excerpt;

    return $the_excerpt;
}
function beau_pagenav($pages = '', $range = 4)
{
    global $loop, $paged;
    $showitems = ($range * 2) + 1;
    if ( empty($paged) ) $paged = 1;
    if ( $pages == '' ) {
        $pages = $loop->max_num_pages;
        if ( !$pages ) {
            $pages = 1;
        }
    }
    if ( 1 != $pages )
    {
        echo '<div class="pagination">';
        if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) echo ' <a href="'.get_pagenum_link(1).'">&laquo;</a>';
        if ( $paged > 1 && $showitems < $pages ) echo ' <a href="'.get_pagenum_link($paged - 1).'">&lsaquo;</a>';
        for ( $i=1; $i <= $pages; $i++ )
        {
            if ( 1 != $pages && (!($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems) )
            {
                echo ( $paged == $i ) ? ' <span class="current">'.$i.'</span>':' <a href="'.get_pagenum_link($i).'" class="inactive" >'.$i.'</a>';
            }
        }
        if ( $paged < $pages && $showitems < $pages ) echo ' <a href="'.get_pagenum_link($paged + 1).'">&rsaquo;</a>';
        if ( $paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages ) echo ' <a href="'.get_pagenum_link($pages).'">&raquo;</a>';
        echo '</div>'."\n";
    }
    echo '<div class="hide">'; posts_nav_link(); echo '</div>';
}
function wp_get_attachment( $attachment_id ) {
    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'link' => $attachment->su_slide_link,
        'title' => $attachment->post_title
    );
}
function get_attachment_id_from_src ($image_src) {

    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return $id;

}
function chrome_fix( ) {
    ?>
    <script type="text/javascript">
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(is_chrome) {
            jQuery(window).load(function(){jQuery('body').width(jQuery('body').width()+1).width('auto')})
        }
    </script>

<?php
}
add_action( 'wp_head', 'chrome_fix');
class Beau_Walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth, $args) {
        if ( isset($item->classes[0]) ) {
            $icon_class = $item->classes[0]; unset($item->classes[0]);
            $icon_class = !empty( $icon_class ) ? '<i class="'. $icon_class .'" ></i> ': '';
            $item->title = $icon_class . $item->title;
        }

        parent::start_el($output, $item, $depth, $args);
    }
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        $id_field = $this->db_fields['id'];
        if ( !empty( $children_elements[ $element->$id_field ] ) ) {
            $element->classes[] = 'menu-parent-item';
        }
        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
}

function beau_tweets_list($consumer_key, $consumer_secret, $access_token, $access_token_secret, $twitterID, $count){
        $consumer_key = !empty($consumer_key) ? $consumer_key : 'EsZNUJOFBDqRqAvhLNSHYY3qK';
        $consumer_secret = !empty($consumer_secret) ? $consumer_secret : 'OjMJ4CrC5kaVN5uejYGGjyI7b0Gpd2L8IwGSys9M7YhpOp8XJA';
        $access_token = !empty($access_token) ? $access_token : '2709113064-lOv1trWVtVCVXyxTCIMAz1c849I4BZAUqgRoGdh';
        $access_token_secret = !empty($access_token_secret) ? $access_token_secret : '6croIj4O2vfrz2hCDP1G7bmEirmKT1ttZ1W6EvZyJTEGO';
        $count = !empty($count) ? $count : 3;
        $twitterID = !empty($twitterID) ? $twitterID : '@Beautheme2014';
        require_once BEAU_BASE .'/function/twitteroauth/twitteroauth.php';
        $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
        $tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$twitterID."&count=".$count);
        ?>
        <div class="latest-tweets">
            <ul>
                <?php foreach($tweets as $tweet){
                    $status = $tweet->text;
                    $status = preg_replace('/http:\/\/([a-z0-9_\.\-\+\&\!\#\~\/\,]+)/i', '<a href="http://$1" target="_blank">http://$1</a>', $status);
                    $status = preg_replace('/@([a-z0-9_]+)/i', '<a href="http://twitter.com/$1" target="_blank">@$1</a>', $status);
                    $timer = strtotime($tweet->created_at);
                    $timer = relative_time($timer);
                    ?>
                    <li>
                        <p class="tweet-text">
                            <?php echo $status; ?>
                        </p>
                            <a style="font-size:85%" href="http://twitter.com/<?php echo $tweet->user->screen_name; ?>/statuses/<?php echo $tweet->id_str; ?>">
                        <p><?php echo $timer; ?>
                        </p>
                        </a>
                    </li>
                <?php
                }?>
            </ul>
        </div>
        <a href="https://twitter.com/intent/follow?screen_name=<?php echo $twitterID; ?>" class="btn">Follow</a>
<?php

}
function get_title( $title, $sep ) {
    global $paged, $page;
    if ( is_feed() )
        return $title;
    $title .= get_bloginfo( 'name' );
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";

    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'beautheme' ), max( $paged, $page ) );

    return $title;
}
add_filter( 'wp_title', 'get_title', 20, 2 );
function relative_time($time)
{
    $gap = time() - $time;

    if ($gap < 5) {
        return 'Less than 5 seconds ago';
    } else if ($gap < 10) {
        return 'Less than 10 seconds ago';
    } else if ($gap < 20) {
        return 'Less than 20 seconds ago';
    } else if ($gap < 40) {
        return 'Half a minute ago';
    } else if ($gap < 60) {
        return 'Less than a minute ago';
    }

    $gap = round($gap / 60);
    if ($gap < 60)  {
        return $gap.' minute'.($gap > 1 ? 's' : '').' ago';
    }

    $gap = round($gap / 60);
    if ($gap < 24)  {
        return 'About '.$gap.' hour'.($gap > 1 ? 's' : '').' ago';
    }

    return date('h:i A F d, Y', $time);
}
?>