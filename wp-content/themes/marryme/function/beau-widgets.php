<?php

/*******************************************************************
Twitter 
********************************************************************/
class beau_twitter extends WP_Widget {

	function beau_twitter()
	{
		$widget_ops = array('classname' => 'widget-twitter', 'description' => 'The most latest twitter messages on your site.' );

		$this->WP_Widget( 'st-twitter', THEMENAME.' - Latest Tweets', $widget_ops );
	}

	function widget( $args, $instance )
	{
		extract($args);

		$title = apply_filters('widget_title', empty($instance['title']) ? __('Latest Tweets', 'beau') : $instance['title'], $instance, $this->id_base);
		$consumer_key = !empty($instance['consumer_key']) ? $instance['consumer_key'] : '';
		$consumer_secret = !empty($instance['consumer_secret']) ? $instance['consumer_secret'] : '';
		$access_token = !empty($instance['access_token']) ? $instance['access_token'] : '';
		$access_token_secret = !empty($instance['access_token_secret']) ? $instance['access_token_secret'] : '';
		$twitterID = !empty($instance['twitterID']) ? $instance['twitterID'] : '';
		$count = !empty($instance['count']) ? $instance['count'] : '';

		echo $before_widget;

		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }
		if ( $consumer_key && $consumer_secret && $access_token && $access_token_secret && $twitterID && $count )
		{
			$transientName = 'st_twitter_'. $args['widget_id'];
			$cacheTime = 15;
			if ( false === ( $twit_data = get_transient($transientName) ) )
			{
				 require_once BEAU_BASE .'/function/twitteroauth/twitteroauth.php';
				 $twit_connect = new TwitterOAuth(
												$consumer_key,		/*Consumer Key*/
												$consumer_secret,   /*Consumer secret*/
												$access_token,      /*Access token*/
												$access_token_secret /*Access token secret*/
												);
				 $twit_data = $twit_connect->get('statuses/user_timeline', array(

																				'screen_name'     => $twitterID,
																				'count'           => $count,
																				'exclude_replies' => false
																				)
																			);
				 if ( $twit_connect->http_code != 200 )
				 {
					  $twit_data = get_transient($transientName);
				 }
				 set_transient($transientName, $twit_data, 60 * $cacheTime);
			}

			$twitter = get_transient($transientName);
			if ( $twitter && is_array($twitter) )
			{
				?>
				<ul class="unstyled" id="twitter_<?php echo $args['widget_id']; ?>">
					<?php
					foreach ( $twitter as $tweet )
					{
						$status = $tweet->text;
						$status = preg_replace('/http:\/\/([a-z0-9_\.\-\+\&\!\#\~\/\,]+)/i', '<a href="http://$1" target="_blank">http://$1</a>', $status);
						$status = preg_replace('/@([a-z0-9_]+)/i', '<a href="http://twitter.com/$1" target="_blank">@$1</a>', $status);

						$timer = strtotime($tweet->created_at);
						$timer = $this->relative_time($timer);
						?>
						<li><span><?php echo $status; ?></span> <a style="font-size:85%" href="http://twitter.com/<?php echo $tweet->user->screen_name; ?>/statuses/<?php echo $tweet->id_str; ?>"><?php echo $timer; ?></a></li>
						<?php
					}
					?>
				</ul>
				<?php
			}
		}
        else{
            echo 'FALSE';
        }
		echo $after_widget;
	}

	function relative_time($time)
	{
		$gap = time() - $time;

		if ($gap < 5) {
			return 'less than 5 seconds ago';
		} else if ($gap < 10) {
			return 'less than 10 seconds ago';
		} else if ($gap < 20) {
			return 'less than 20 seconds ago';
		} else if ($gap < 40) {
			return 'half a minute ago';
		} else if ($gap < 60) {
			return 'less than a minute ago';
		}

		$gap = round($gap / 60);
		if ($gap < 60)  {
			return $gap.' minute'.($gap > 1 ? 's' : '').' ago';
		}

		$gap = round($gap / 60);
		if ($gap < 24)  {
			return 'about '.$gap.' hour'.($gap > 1 ? 's' : '').' ago';
		}

		return date('h:i A F d, Y', $time);
	}

	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['consumer_key'] = strip_tags($new_instance['consumer_key']);
		$instance['consumer_secret'] = strip_tags($new_instance['consumer_secret']);
		$instance['access_token'] = strip_tags($new_instance['access_token']);
		$instance['access_token_secret'] = strip_tags($new_instance['access_token_secret']);
		$instance['twitterID'] = strip_tags($new_instance['twitterID']);
		$instance['count'] = strip_tags($new_instance['count']);
		return $instance;
	}

	function form( $instance )
	{
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'twitterID' => '', 'count' => 5 ) );
		$title = strip_tags($instance['title']);
		$consumer_key = strip_tags($instance['consumer_key']);
		$consumer_secret = strip_tags($instance['consumer_secret']);
		$access_token = strip_tags($instance['access_token']);
		$access_token_secret = strip_tags($instance['access_token_secret']);
		$twitterID = strip_tags($instance['twitterID']);
		$count = strip_tags($instance['count']);

		?>
		<p><a href="http://dev.twitter.com/apps">Find or Create your Twitter App</a></p>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('consumer_key'); ?>">Consumer Key:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('consumer_key'); ?>" name="<?php echo $this->get_field_name('consumer_key'); ?>" type="text" value="<?php echo esc_attr($consumer_key); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('consumer_secret'); ?>">Consumer Secret:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('consumer_secret'); ?>" name="<?php echo $this->get_field_name('consumer_secret'); ?>" type="text" value="<?php echo esc_attr($consumer_secret); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('access_token'); ?>">Access Token:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('access_token'); ?>" name="<?php echo $this->get_field_name('access_token'); ?>" type="text" value="<?php echo esc_attr($access_token); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('access_token_secret'); ?>">Access Token Secret:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('access_token_secret'); ?>" name="<?php echo $this->get_field_name('access_token_secret'); ?>" type="text" value="<?php echo esc_attr($access_token_secret); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('twitterID'); ?>">Twitter ID:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('twitterID'); ?>" name="<?php echo $this->get_field_name('twitterID'); ?>" type="text" value="<?php echo esc_attr($twitterID); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('count'); ?>">Number to display:</label>
			<select id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>">
				<?php
				for ( $i = 1; $i <= 10; $i++ )
				{
					$selected = ($count == $i) ? ' selected' : '';

					echo '<option value="'.$i.'"'. $selected .'>'.$i.'</option>';
				}
				?>
			</select>
		</p>
		<?php
	}
}


/*******************************************************************
Flick
********************************************************************/
class beau_flickr extends WP_Widget {

	function beau_flickr()
	{
		$widget_ops = array('classname' => 'widget-flickr', 'description' => 'The most latest flickr photo on your site.' );

		$this->WP_Widget( 'st-flickr', THEMENAME.' - Latest Flickr Photos', $widget_ops );
	}

	function widget( $args, $instance )
	{
		extract($args);
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Latest Flickr Photos', 'beau') : $instance['title'], $instance, $this->id_base);
		$flickrID = !empty($instance['flickrID']) ? $instance['flickrID'] : '';
		$count = !empty($instance['count']) ? $instance['count'] : '';
		$display = !empty($instance['display']) ? $instance['display'] : '';
		
		echo $before_widget;
		
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }
		
		?>
		<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo $count; ?>&amp;display=<?php echo $display; ?>&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo $flickrID; ?>"></script>
		<?php
		
		echo $after_widget;
	}

	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['flickrID'] = strip_tags($new_instance['flickrID']);
		$instance['count'] = strip_tags($new_instance['count']);
		$instance['display'] = strip_tags($new_instance['display']);
		return $instance;
	}

	function form( $instance )
	{		
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'flickrID' => '', 'display' => 'latest', 'count' => 9 ) );
		$title = strip_tags($instance['title']);
		$flickrID = strip_tags($instance['flickrID']);
		$count = strip_tags($instance['count']);
		$display = strip_tags($instance['display']);
		
		$display_opt = array('latest', 'random');
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('flickrID'); ?>">Flickr ID:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('flickrID'); ?>" name="<?php echo $this->get_field_name('flickrID'); ?>" type="text" value="<?php echo esc_attr($flickrID); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('display'); ?>">What pictures to display:</label>
			<select class="widefat" id="<?php echo $this->get_field_id('display'); ?>" name="<?php echo $this->get_field_name('display'); ?>">
				<?php
				foreach ( $display_opt as $opt )
				{					
					$selected = ($display == $opt) ? ' selected' : '';
				
					echo '<option value="'.$opt.'"'. $selected .'>'.ucfirst($opt).'</option>';
				}
				?>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('count'); ?>">Number to display:</label>
			<select id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>">
				<?php
				for ( $i = 1; $i <= 10; $i++ )
				{					
					$selected = ($count == $i) ? ' selected' : '';
				
					echo '<option value="'.$i.'"'. $selected .'>'.$i.'</option>';
				}
				?>
			</select>
		</p>
		<?php
	}
}
class Beau_Recent_Posts_Width_FI extends WP_Widget {

    function __construct() {
        $widget_ops = array('classname' => 'widget_recent_entries', 'description' => __( "The most recent posts on your site",'marryme') );
        parent::__construct('recent-posts', __('Recent Posts Blog','marryme'), $widget_ops);
        $this->alt_option_name = 'widget_recent_entries';

        add_action( 'save_post', array($this, 'flush_widget_cache') );
        add_action( 'deleted_post', array($this, 'flush_widget_cache') );
        add_action( 'switch_theme', array($this, 'flush_widget_cache') );
    }

    function widget($args, $instance) {
        $cache = wp_cache_get('widget_recent_posts', 'widget');

        if ( !is_array($cache) )
            $cache = array();

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        ob_start();
        extract($args);

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts','marryme' );
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
        if ( ! $number )
        $number = 10;
        $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
        if ($r->have_posts()) :
            ?>
            <?php echo $before_widget; ?>
            <?php if ( $title ) echo $before_title . $title . $after_title; ?>
            <div class="recent-post">
                <?php while ( $r->have_posts() ) : $r->the_post();
                        $featuredID =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    ?>
                    <div class="row-fluid">
                        <div class="span4"><img src="<?php echo $featuredID[0];?>" alt="<?php echo get_the_title();?>"></div>
                        <div class="span7">
                            <div class="recent-post-title"><?php echo get_the_author();?></div>
                            <div class="recent-post-des">
                                <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID() ); ?>">
                                    <?php if ( get_the_title() ) the_title(); else the_ID();
                                    ?></a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <?php echo $after_widget; ?>
            <?php
            wp_reset_postdata();
        endif;
        $cache[$args['widget_id']] = ob_get_flush();
        wp_cache_set('widget_recent_posts', $cache, 'widget');
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        $instance['show_date'] = (bool) $new_instance['show_date'];
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_recent_entries']) )
            delete_option('widget_recent_entries');

        return $instance;
    }

    function flush_widget_cache() {
        wp_cache_delete('widget_recent_posts', 'widget');
    }

    function form( $instance ) {
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ,'marryme'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:','marryme' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

        <p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?','marryme' ); ?></label></p>
    <?php
    }
}

/**
 * Recent_Comments widget class
 *
 * @since 2.8.0
 */
class Beau_Widget_Recent_Comments extends WP_Widget {

    public function __construct() {
        $widget_ops = array('classname' => 'widget_recent_comments', 'description' => __( 'Your site&#8217;s most recent comments.','marryme'  ));
        parent::__construct('recent-comments', __('Recent Comments','marryme'), $widget_ops);
        $this->alt_option_name = 'widget_recent_comments';

        if ( is_active_widget(false, false, $this->id_base) )
            add_action( 'wp_head', array($this, 'recent_comments_style') );

        add_action( 'comment_post', array($this, 'flush_widget_cache') );
        add_action( 'edit_comment', array($this, 'flush_widget_cache') );
        add_action( 'transition_comment_status', array($this, 'flush_widget_cache') );
    }

    public function recent_comments_style() {

        /**
         * Filter the Recent Comments default widget styles.
         *
         * @since 3.1.0
         *
         * @param bool   $active  Whether the widget is active. Default true.
         * @param string $id_base The widget ID.
         */
        if ( ! current_theme_supports( 'widgets' ) /*Temp hack #14876*/
            || ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) )
            return;
        ?>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <?php
    }

    public function flush_widget_cache() {
        wp_cache_delete('widget_recent_comments', 'widget');
    }

    public function widget( $args, $instance ) {
        global $comments, $comment;

        $cache = array();
        if ( ! $this->is_preview() ) {
            $cache = wp_cache_get('widget_recent_comments', 'widget');
        }
        if ( ! is_array( $cache ) ) {
            $cache = array();
        }

        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        $output = '';

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments','marryme' );

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;

        /**
         * Filter the arguments for the Recent Comments widget.
         *
         * @since 3.4.0
         *
         * @see get_comments()
         *
         * @param array $comment_args An array of arguments used to retrieve the recent comments.
         */
        $comments = get_comments( apply_filters( 'widget_comments_args', array(
            'number'      => $number,
            'status'      => 'approve',
            'post_status' => 'publish'
        ) ) );

        $output .= $args['before_widget'];
        if ( $title ) {
            $output .= $args['before_title'] . $title . $args['after_title'];
        }

        $output .= ' <div class="recent-comment" id="recentcomments">';
        if ( $comments ) {
            $post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
            _prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );
            foreach ( (array) $comments as $comment) {
                $output .= ' <div class="row-fluid recentcomments">';
                    $output.='<div class="span3">'.get_avatar($comments->comment_ID).'</div>';
                    $output.='<div class="span9">';
                        $output.='<div class="recent-post-des">';
                            $output.='<a href="' . esc_url( get_comment_link( $comment->comment_ID ) ) . '">' . get_the_title( $comment->comment_post_ID ) . '</a>';
                        $output.='</div>';
                    $output.='</div>';
                $output.='</div>';
            }
        }
        $output .= '</div>';
        $output .= $args['after_widget'];

        echo $output;

        if ( ! $this->is_preview() ) {
            $cache[ $args['widget_id'] ] = $output;
            wp_cache_set( 'widget_recent_comments', $cache, 'widget' );
        }
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = absint( $new_instance['number'] );
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_recent_comments']) )
            delete_option('widget_recent_comments');

        return $instance;
    }

    public function form( $instance ) {
        $title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ,'marryme'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of comments to show:','marryme' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
    <?php
    }
}
function beau_register_widgets() {
	register_widget( 'beau_twitter' );
	register_widget( 'beau_flickr' );
	register_widget( 'Beau_Recent_Posts_Width_FI' );
	register_widget( 'Beau_Widget_Recent_Comments' );
}

add_action( 'widgets_init', 'beau_register_widgets' );