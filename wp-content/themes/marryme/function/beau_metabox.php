<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 10/6/14
 * Time: 10:57 PM
 * To change this template use File | Settings | File Templates.
 */

add_filter( 'cmb_meta_boxes', 'blog_metaboxes' );
add_filter( 'cmb_meta_boxes', 'page_metaboxes' );
add_filter( 'cmb_meta_boxes', 'couple_metaboxes' );
add_filter( 'cmb_meta_boxes', 'event_metaboxes' );
add_filter( 'cmb_meta_boxes', 'slider_metaboxes' );

function page_metaboxes( array $meta_boxes ) {
    $prefix = '_page_';
    $meta_boxes[] = array(
        'id'         => 'general_metabox',
        'title'      => 'General Options',
        'pages'      => array( 'page' ), /*Post type*/
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, /*Show field names on the left*/
        'fields'     => array(
            array(
                'name'    => 'Page Template',
                'desc'    => '',
                'id'      => $prefix .'template',
                'type'    => 'select',
                'std'     => 'none',
                'options' => array(
                    array( 'value' => 'blog', 'name' => 'Blog Template' ),
                    array( 'value' => 'blog-full', 'name' => 'Blog Full Layout Template' ),
                    array( 'value' => 'blog-left', 'name' => 'Blog Left Sidebar Template'),
                    array( 'value' => 'blog-right', 'name' => 'Blog Right Sidebar Template'),
                    array( 'value' => 'home', 'name' => 'Home Template' ),
                    array( 'value' => 'event', 'name' => 'Event Template' ),
                    array( 'value' => 'gallery', 'name' => 'Gallery Template' ),
                    array( 'value' => 'shortcode', 'name' => 'Shortcode Template' ),
                    array( 'value' => 'contact', 'name' => 'Contact Template' ),
                    array( 'value' => 'guest-book', 'name' => 'Guest book Template' ),
                ),
            ),
            array(
                'name'    => 'Gallery Layout',
                'desc'    => '',
                'id'      => $prefix .'gallery_layout',
                'type'    => 'select',
                'options' => array(
                    array( 'value' => '2', 'name' => '2 Columns' ),
                    array( 'value' => '3', 'name' => '3 Columns' ),
                    array( 'value' => '4', 'name' => '4 Columns' ),
                ),
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'home_builder',
        'title'      => 'Home Builder Options',
        'pages'      => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name'    => 'Home slider',
                'desc'    => 'Chose home slider type',
                'id'      => $prefix .'silder_home',
                'type'    => 'select',
                'options' => array(
                    array( 'value' => 'image', 'name' => 'Image and Video Slider' ),
                    array( 'value' => 'youtube', 'name' => 'Youtube Background Video' ),
                    array( 'value' => 'vimeo', 'name' => 'Vimeo Background Video' ),
                ),
                'std'     => 'none',
            ),
            array(
                'name'    => 'Video ID',
                'desc'    => '</br>Paste the ID of the video is hosted on, such as Youtube and Vimeo. you want to show <br>E.g. http://www.youtube.com/watch?v=<strong style="color:red;">tbPhf_KXNZI</strong><br>http://vimeo.com/<strong style="color:red;">6428069</strong>',
                'id'      => $prefix .'videoID',
                'type'    => 'text_medium',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Text on video background',
                'desc'    => 'Text on video background',
                'id'      => $prefix .'text_videoID',
                'type'    => 'wysiwyg',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show couple',
                'desc'    => 'Show couple on home page',
                'id'      => $prefix .'couple',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show couple',
                'desc'    => 'Show couple on home page',
                'id'      => $prefix .'couple',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show Time Count',
                'desc'    => 'Show time count on home page',
                'id'      => $prefix .'time',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Time Count Text Header',
                'desc'    => 'Time count text teader on home page',
                'id'      => $prefix .'time_text',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Time Count',
                'desc'    => 'Time count on home page',
                'id'      => $prefix .'time_count',
                'type'    => 'text_date',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show Event',
                'desc'    => 'Show event on home page',
                'id'      => $prefix .'event',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Event text header ',
                'desc'    => 'Event home header on home page',
                'id'      => $prefix .'event_text',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Event descriptions',
                'desc'    => 'Event home header on home page',
                'id'      => $prefix .'event_des',
                'type'    => 'textarea',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show Tweets',
                'desc'    => 'Show tweets on home page',
                'id'      => $prefix .'tweet',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Tweets text header ',
                'desc'    => 'Tweets home header on home page',
                'id'      => $prefix .'tweet_text',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Tweets question',
                'desc'    => 'Tweets home question on home page',
                'id'      => $prefix .'tweet_question',
                'type'    => 'textarea',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Tweets author header ',
                'desc'    => 'Tweets home header on home page',
                'id'      => $prefix .'tweet_author',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show album gallery',
                'desc'    => 'Show album gallery on home page',
                'id'      => $prefix .'album',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Album text header ',
                'desc'    => 'Album gallery header on home page',
                'id'      => $prefix .'album_text',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Album description',
                'desc'    => 'Album description gallery on home page',
                'id'      => $prefix .'album_des',
                'type'    => 'text_medium',
                'std'     => 'none',
            ),

            array(
                'name'    => 'Show List Blog',
                'desc'    => 'Show list blog on home page',
                'id'      => $prefix .'blog',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Blog text header ',
                'desc'    => 'Album gallery header on home page',
                'id'      => $prefix .'blog_text',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Blog description',
                'desc'    => 'Blog description gallery on home page',
                'id'      => $prefix .'blog_des',
                'type'    => 'text_medium',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Show Attending Form',
                'desc'    => 'Show attending form on home page',
                'id'      => $prefix .'attending',
                'type'    => 'checkbox',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Blog text header ',
                'desc'    => 'Album gallery header on home page',
                'id'      => $prefix .'attending_text',
                'type'    => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Attending description',
                'desc'    => 'Attending description gallery on home page',
                'id'      => $prefix .'attending_des',
                'type'    => 'text_medium',
                'std'     => 'none',
            )
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'event_metabox',
        'title'      => 'Event Options',
        'pages'      => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name'    => 'Event Text Header',
                'desc'    => 'Text hear on list event page',
                'id'      => $prefix .'event_list_header',
                'type' => 'text',
                'std'     => 'none',
            ),
            array(
                'name'    => 'Event Text Header',
                'desc'    => 'Text description on list event page',
                'id'      => $prefix .'event_list_des',
                'type' => 'textarea',
                'std'     => 'none',
            ),
        ),
    );
    return $meta_boxes;
}
function blog_metaboxes( array $meta_boxes ) {
    $prefix = '_blog_';
    $meta_boxes[] = array(
        'id'         => 'blog_metaboxes',
        'title'      => 'General Options',
        'pages'      => array( 'post' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name' => 'Location Meta',
                'id'   => $prefix .'location',
                'type' => 'text_large',
            ),
        ),
    );

    return $meta_boxes;
}
function slider_metaboxes( array $meta_boxes ) {
    $prefix = '_slider_';
    $meta_boxes[] = array(
        'id'         => 'slider_metaboxes',
        'title'      => 'General Options',
        'pages'      => array( 'slider' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name' => 'Type Slider',
                'type'    => 'select',
                'id'      => $prefix .'type',
                'options' => array(
                    array( 'value' => 'image', 'name' => 'Image' ),
                    array( 'value' => 'youtube', 'name' => 'Youtube' ),
                    array( 'value' => 'vimeo', 'name' => 'Vimeo' ),
                ),
            ),
            array(
                'name' => 'Show date',
                'type'    => 'select',
                'id'      => $prefix .'date',
                'options' => array(
                    array( 'value' => 'yes', 'name' => 'Yes' ),
                    array( 'value' => 'no', 'name' => 'No' ),
                ),
            ),
            array(
                'name' => 'Video ID',
                'type'    => 'text_medium',
                'id'      => $prefix .'videoid',
                'desc'    => '</br>Paste the ID of the video is hosted on, such as Youtube and Vimeo. you want to show <br>E.g. http://www.youtube.com/watch?v=<strong style="color:red;">tbPhf_KXNZI</strong><br>http://vimeo.com/<strong style="color:red;">6428069</strong>',
            ),
        ),
    );

    return $meta_boxes;
}
function couple_metaboxes( array $meta_boxes ) {
    $prefix = '_couple_';
    $meta_boxes[] = array(
        'id'         => 'couple_metaboxes',
        'title'      => 'General Options',
        'pages'      => array( 'couple' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name'    => 'Ages',
                'desc'    => '',
                'id'      => $prefix .'age',
                'type'    => 'select',
                'options' => array(
                    array( 'value' => '15', 'name' => '15' ),
                    array( 'value' => '16', 'name' => '16' ),
                    array( 'value' => '17', 'name' => '17' ),
                    array( 'value' => '18', 'name' => '18' ),
                    array( 'value' => '19', 'name' => '19' ),
                    array( 'value' => '20', 'name' => '20' ),
                    array( 'value' => '21', 'name' => '21' ),
                    array( 'value' => '22', 'name' => '22' ),
                    array( 'value' => '23', 'name' => '23' ),
                    array( 'value' => '24', 'name' => '24' ),
                    array( 'value' => '25', 'name' => '25' ),
                    array( 'value' => '26', 'name' => '26' ),
                    array( 'value' => '27', 'name' => '27' ),
                    array( 'value' => '28', 'name' => '28' ),
                    array( 'value' => '29', 'name' => '29' ),
                    array( 'value' => '30', 'name' => '31' ),
                    array( 'value' => '30', 'name' => '32' ),
                    array( 'value' => '30', 'name' => '33' ),
                    array( 'value' => '30', 'name' => '34' ),
                    array( 'value' => '30', 'name' => '35' ),
                    array( 'value' => '30', 'name' => '36' ),
                    array( 'value' => '30', 'name' => '37' ),
                    array( 'value' => '30', 'name' => '38' ),
                    array( 'value' => '30', 'name' => '39' ),
                    array( 'value' => '30', 'name' => '40' ),
                    array( 'value' => '30', 'name' => '41' ),
                    array( 'value' => '30', 'name' => '42' ),
                    array( 'value' => '30', 'name' => '43' ),
                    array( 'value' => '30', 'name' => '44' ),
                    array( 'value' => '30', 'name' => '45' ),
                    array( 'value' => '30', 'name' => '46' ),
                    array( 'value' => '30', 'name' => '47' ),
                    array( 'value' => '30', 'name' => '48' ),
                    array( 'value' => '30', 'name' => '49' ),
                    array( 'value' => '30', 'name' => '50' ),
                    array( 'value' => '30', 'name' => '51' ),
                    array( 'value' => '30', 'name' => '52' ),
                    array( 'value' => '30', 'name' => '53' ),
                    array( 'value' => '30', 'name' => '54' ),
                    array( 'value' => '30', 'name' => '55' ),
                    array( 'value' => '30', 'name' => '56' ),
                    array( 'value' => '30', 'name' => '57' ),
                    array( 'value' => '30', 'name' => '58' ),
                    array( 'value' => '30', 'name' => '59' ),
                    array( 'value' => '30', 'name' => '60' ),
                    array( 'value' => '30', 'name' => '60' ),
                    array( 'value' => '30', 'name' => '62' ),
                    array( 'value' => '30', 'name' => '63' ),
                    array( 'value' => '30', 'name' => '64' ),
                    array( 'value' => '30', 'name' => '65' ),
                    array( 'value' => '30', 'name' => '66' ),
                    array( 'value' => '30', 'name' => '67' ),
                    array( 'value' => '30', 'name' => '68' ),
                    array( 'value' => '30', 'name' => '69' ),
                    array( 'value' => '30', 'name' => '70' ),
                    array( 'value' => '30', 'name' => '71' ),
                    array( 'value' => '30', 'name' => '72' ),
                    array( 'value' => '30', 'name' => '73' ),
                    array( 'value' => '30', 'name' => '74' ),
                    array( 'value' => '30', 'name' => '75' ),
                    array( 'value' => '30', 'name' => '76' ),
                    array( 'value' => '30', 'name' => '77' ),
                    array( 'value' => '30', 'name' => '78' ),
                    array( 'value' => '30', 'name' => '79' ),
                    array( 'value' => '30', 'name' => '80' ),
                ),
            ),
            array(
                'name' => 'Facebook ID',
                'id'   => $prefix .'facebook',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'Twitter ID',
                'id'   => $prefix .'twitter',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'Google Plus ID',
                'id'   => $prefix .'google',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'In ID',
                'id'   => $prefix .'pinterest',
                'type' => 'text_medium',
            ),
        ),
    );
    return $meta_boxes;
}
function event_metaboxes( array $meta_boxes ) {
    $prefix = '_event_';
    $meta_boxes[] = array(
        'id'         => 'event_general_metaboxes',
        'title'      => 'General Options',
        'pages'      => array( 'event' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name' => 'Location Meta',
                'id'   => $prefix .'place',
                'type' => 'text',
            ),
            array(
                'name' => 'Location',
                'id'   => $prefix .'location',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'Date',
                'id'   => $prefix .'date',
                'type' => 'text_date',
            ),
            array(
                'name' => 'Start Time',
                'id'   => $prefix .'time_start',
                'type' => 'text_time',
            ),
            array(
                'name' => 'End Time',
                'id'   => $prefix .'time_end',
                'type' => 'text_time',
            ),
        ),
    );
    $meta_boxes[] = array(
        'id'         => 'event_map_metaboxes',
        'title'      => 'Google map',
        'pages'      => array( 'event' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name' => 'Zoom',
                'id'   => $prefix .'zoom',
                'type'    => 'select',
                'std'     => 'none',
                'options' => array(
                    array( 'value' => '10', 'name' => '10' ),
                    array( 'value' => '11', 'name' => '11' ),
                    array( 'value' => '12', 'name' => '12' ),
                    array( 'value' => '13', 'name' => '13' ),
                    array( 'value' => '14', 'name' => '14' ),
                    array( 'value' => '15', 'name' => '15'),
                    array( 'value' => '16', 'name' => '16'),
                    array( 'value' => '17', 'name' => '17' ),
                    array( 'value' => '18', 'name' => '18' ),
                    array( 'value' => '19', 'name' => '19' ),
                    array( 'value' => '20', 'name' => '20' ),
                ),
            ),
            array(
                'name' => 'Address',
                'id'   => $prefix .'address',
                'type' => 'text',
            ),
            array(
                'name' => 'Infowindow',
                'id'   => $prefix .'google',
                'type' => 'text',
            ),
            array(
                'name' => 'Latitude',
                'id'   => $prefix .'latitude',
                'type' => 'text',
            ),
            array(
                'name' => 'Longitude',
                'id'   => $prefix .'longitude',
                'type' => 'text',
            ),
        ),
    );
    return $meta_boxes;
}
function gallery_metaboxes( array $meta_boxes ) {
    $prefix = '_gallery_';
    $meta_boxes[] = array(
        'id'         => 'gallery_metaboxes',
        'title'      => 'General Options',
        'pages'      => array( 'event' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'fields'     => array(
            array(
                'name' => 'Location',
                'id'   => $prefix .'location',
                'type' => 'text_medium',
            ),
            array(
                'name' => 'Date',
                'id'   => $prefix .'date',
                'type' => 'text_date_timestamp',
            ),
            array(
                'name' => 'Start Time',
                'id'   => $prefix .'time_start',
                'type' => 'text_time',
            ),
            array(
                'name' => 'End Time',
                'id'   => $prefix .'time_end',
                'type' => 'text_time',
            ),
        ),
    );
    return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
function cmb_initialize_cmb_meta_boxes() {
    if ( ! class_exists( 'cmb_Meta_Box' ) )
        require_once BEAU_ADMIN .'/metaboxes/init.php';
}