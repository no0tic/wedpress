<?php
define('BEAU_BASE_URL', get_template_directory_uri());
define('BEAU_BASE', get_template_directory());

define('BEAU_JS', BEAU_BASE_URL . '/js');
define('BEAU_CSS', BEAU_BASE_URL . '/css');
define('BEAU_IMAGES', BEAU_BASE_URL . '/images');
define('BEAU_ADMIN', BEAU_BASE . '/admin');
/*
 * Loads the Options Panel
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */

define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/admin/' );
require_once dirname( __FILE__ ) . '/admin/options-framework.php';
require_once ('include/scripts/aq_resizer.php');
require_once ('include/scripts/tgm-plugins.php');
require_once ('function/beau-function.php');
require_once ('function/beau-widgets.php');
require_once ('function/beau_metabox.php');
