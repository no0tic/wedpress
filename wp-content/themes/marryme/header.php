<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<!-- Basic Page Needs
  	================================================== -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
	<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Mobile Specific Metas
  	================================================== -->
	<link rel="shortcut icon" href="<?php echo of_get_option('site_favicon'); ?>" type="image/x-icon" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php
    if( empty( $op['enabled_loading_screen'] ) ){?>
        <style>
            body{
                overflow-y: visible;;
            }
        </style>
    <?php }?>
    <?php wp_head(); ?>
</head>
<?php
    global $style;
    $style=of_get_option('blog_style');
    $class="";
    $header ="";
    $style = !empty($style) ? $style : 1;
    switch ($style):
        case 1:
            $class='style1';
            $header ="style-1";
            break;
        case 2:
            $class="style2";
            $header ="style-2";
        break;
    endswitch;
    $op = get_option('loading_page_options');
    $page_silder_home = get_post_meta(get_the_ID(), '_page_silder_home', TRUE);
    ?>
<body <?php body_class($class);?>>
<div id="st-wrapper">
    <header id="header" class="row-fluid beau-animated move-to-bottom">
        <?php if($page_silder_home!='image'){?>
            <div class="beau-header-bg-style style-2"></div>
        <?php }?>
        <div class="beau-header">
            <?php
            if (!is_front_page()){
               $p='position: relative';
            }
            ?>
        <div class="beau-header-top" style="<?php echo $p;?>">
            <nav id="primary-nav">
                <a id="toggle-menu"><span class="toggle-icon"></span></a>
                <?php wp_nav_menu( array( 'menu_id' => 'primary-menu', 'menu_class' => 'mobile-primary inline', 'theme_location' => 'mobile-primary')); ?>
                <div class="beau-social">
                    <ul class="inline ">
                        <li><a class="social-facebook" href="<?php echo of_get_option('soc_facebook');?>" target="_blank"></a></li>
                        <li><a class="social-twitter" href="<?php echo of_get_option('soc_twitter');?>" target="_blank"></a></li>
                        <li><a class="social-pinterest" href="<?php echo of_get_option('soc_pinterest');?>" target="_blank"></a></li>
                        <li><a class="social-google"  href="<?php echo of_get_option('soc_google');?>" target="_blank"></a></li>
                        <li><a class="mobile-search-icon sticky-open" href="#"></a></li>
                    </ul>
                </div>
            </nav>
            <div class="row-fluid">
                <div class="span4">
                    <div class="beau-social">
                        <ul class="inline ">
                            <li><a class="social-facebook" href="<?php echo of_get_option('soc_facebook');?>" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li><a class="social-twitter" href="<?php echo of_get_option('soc_twitter');?>" target="_blank"></a></li>
                            <li><a class="social-pinterest" href="<?php echo of_get_option('soc_pinterest');?>" target="_blank"></a></li>
                            <li><a class="social-google"  href="<?php echo of_get_option('soc_google');?>" target="_blank"></a></li>
                        </ul>
                    </div>
                    <nav id="primary-menu-left" class="cl-effect-11">
                        <?php wp_nav_menu( array( 'menu_id' => 'left-primary', 'menu_class' => 'primary-menu inline', 'theme_location' => 'left-primary' ) ); ?>
                    </nav>
                </div>
                <div class="span4 logo"><img src="<?php echo of_get_option('site_logo'); ?> " alt="<?php bloginfo('name');?>"></div>
                <div class="span4">
                    <div class="beau-search">
                        <div class="beau-search-icon"></div>
                        <form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
                            <input type="text" name="s" id="s" onblur="if (this.value == '') {this.value=this.defaultValue}" onfocus="if (this.value == this.defaultValue) {this.value = '';}" value="<?php _e('Search...', 'slicetheme') ?>" />
                        </form>
                    </div>
                    <nav id="primary-menu-right">
                        <?php wp_nav_menu( array( 'menu_id' => 'right-primary', 'menu_class' => 'primary-menu inline', 'theme_location' => 'right-primary' ) ); ?>
                    </nav>
                </div>
            </div>
        </div>
        <?php
            $style_header = of_get_option('style_header', 1);
            if ( is_front_page()){
                get_template_part( 'include/header/header-'.$style_header );
            }else{
                ?>
            <?php beau_breadcrumb(); ?>
            <?php
            }
        ?>
    </div>
    </header>
    <?php
    global $arr;
    if(!empty($arr)){?>
    <div class="beau-getting-married modal-container">
    <?php

    foreach ($arr as $id) {
    $typePost= get_post_meta($id, '_slider_type', TRUE);
    ?>
    <div id="myModal-<?php echo $id;?>" class="modal fade">
        <?php
        $youtube= get_post_meta($id, '_slider_videoid', TRUE);
        if($typePost=='vimeo'){
            ?>
            <iframe allowfullscreen="" src="http://player.vimeo.com/video/<?php echo $youtube?>?autoplay=1&amp;loop=1&amp;rel=0&amp;hd=1" width="500" height="281" style="border:0;"></iframe>
        <?php }else{
            echo '<iframe width="560" height="315" src="//www.youtube.com/embed/'. $youtube .'?badge=0&amp;autoplay=0&amp;html5=1" style="border:0;" allowfullscreen="" ></iframe>';
        }?>
    </div>
<?php     } echo '</div>';
}?>