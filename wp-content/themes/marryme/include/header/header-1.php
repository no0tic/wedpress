<?php
    global $imgsize, $style, $arr;
    $page_silder_home = get_post_meta(get_the_ID(), '_page_silder_home', TRUE);
?>
<section id="slider-wrapper">
    <div class="row-fluid">

                <?php if($page_silder_home=='image'){?>
                    <div class="span12">
                    <div class="flexslider flex-fade" >
                        <?php
                        $time_count = get_post_meta(get_the_ID(), '_page_time_count', TRUE);
                        $date = strtotime($time_count);
                        $args = array(
                            'post_type' => 'slider',
                            'posts_per_page' => 99,
                        );
                        $loop = new WP_Query($args);
                        ob_start();
                        if ($loop->have_posts()):
                            $i=0;
                            $arrID=array();
                                if($page_silder_home=='image'){
                                    ?>
                                    <div class="slides-matt style<?php echo $style;?>" ></div>
                                <?php
                                }
                            ?>
                            <ul class="slides">
                                <?php while ($loop->have_posts()) : $loop->the_post();
                                $featuredID =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                                $slider_type = get_post_meta(get_the_ID(), '_slider_type', TRUE);
                                $showdate = get_post_meta(get_the_ID(), '_slider_date', TRUE);
                                $class='';
                                ?>
                                <li>
                                    <?php
                                    if ( $slider_type != 'image' ) {
                                        $arr[$i]=$post->ID;
                                        $i++;?>
                                    <a class="zoom" title="<?php the_title();?>" data-toggle="modal" data-target="#myModal-<?php echo $post->ID;?>">
                                       <?php if($showdate=='yes'){
                                            $class='bottom-typo';
                                        }?>
                                        <div class="icon-video <?php echo $class;?>"><img src="<?php echo BEAU_IMAGES?>/icons/icon-video.png" alt="<?php the_title();?>"></div>
                                    <?php
                                        beau_resizer($featuredID[0], $imgsize['slider']['w'], $imgsize['slider']['h'], true,get_the_title());
                                    ?></a><?php
                                    }else{
                                        beau_resizer($featuredID[0], $imgsize['slider']['w'], $imgsize['slider']['h'], true,get_the_title());
                                    } ?>
                                    <div class="slider-text">
                                            <?php
                                            $thecontent=get_the_content();
                                            if($showdate=='yes'){
                                                if(empty($thecontent)){?>
                                                    <div class="span12 left-text">
                                                        <div class="text-top"><?php echo date( 'l', $date ).' '.date( 'F ', $date ).' '.date( 'j', $date );?></div>
                                                        <div class="text-year"><?php echo date( 'Y ', $date );?></div>
                                                    </div>
                                                <?php   }else{
                                                    ?>
                                                    <div class="span6 left-text">
                                                        <div class="text-top"><?php echo date( 'l', $date ).' '.date( 'F ', $date ).' '.date( 'j', $date );?></div>
                                                        <div class="text-year"><?php echo date( 'Y ', $date );?></div>
                                                    </div>
                                                    <div class="span6 right-text">
                                                        <?php the_content();?>
                                                    </div>
                                                <?php }
                                            }
                                            else{
                                                if(!empty($thecontent)){
                                                    ?>
                                                    <div class="span12 right-text">
                                                        <?php the_content();?>
                                                    </div>
                                                <?php }
                                            }?>
                                        </div>
                                    <?php

                                    endwhile;
                                    ?>
                                </li>
                            </ul>
                            <?php
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
            </div>
                <?php }elseif($page_silder_home=='youtube'){ ?>
        <div class="span12">
                    <div class="text-slider-video"><?php echo get_post_meta(get_the_ID(), '_page_text_videoID', TRUE)?></div>
                        <iframe id="#video" style="border:0;" height="550" width="1368" src="https://www.youtube.com/embed/<?php echo get_post_meta(get_the_ID(), '_page_videoID', TRUE)?>?autoplay=1&amp;controls=0&amp;loop=1&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent&amp;hd=1"></iframe>
        </div>
               <?php }elseif($page_silder_home=='vimeo') {?>
        <div class="span12 frame-vimeo">
                    <iframe style="border:0;" id="player_<?php echo $i+1?>" src="http://player.vimeo.com/video/<?php echo get_post_meta(get_the_ID(), '_page_videoID', TRUE)?>?autoplay=1&amp;controls=0&amp;loop=1&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;wmode=transparent&amp;hd=1"></iframe>
        </div>     <?php }?>
        </div>
       </section>

