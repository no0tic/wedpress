<?php
/**
 * The main template file.
 *
 * This theme is purely for the purpose of testing theme options in Options Framework plugin.
 *
 * @package WordPress
 * @subpackage Options Framework Theme
 */
get_header(); ?>
<?php
    $style=of_get_option('blog_style');
    $part="style-1";
    switch ($style):
        case 1:
            $part="style-1";
            break;
        case 2:
            $part="style-2";
            break;
            break;
    endswitch;
    $loop = is_search() ? 'search' : 'blog-full-layout';
    get_template_part(  $part.'/temp', $loop );
?>
<?php get_footer(); ?>