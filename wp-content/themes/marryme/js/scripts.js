
jQuery(function($) {
	"use strict";
    $('.flex-event').flexslider({
        slideshow: false,
        smoothHeight: true,
        pauseOnHover: true,
        animation: "fade",
        manualControls: ".flex-control-nav li",
        directionNav: false,         /*//Boolean: Create navigation for previous/next navigation? (true/false)*/
        pausePlay: true,
        pauseText: '',
        playText: ''
    });
    $('.flex-fade').flexslider({
        smoothHeight: true,
		pauseOnHover: true,
		animation: "fade",
        controlNav: false,
        animationSpeed:1500
    });
    $('.flex-event').flexslider({
        smoothHeight: true,
        pauseOnHover: true,
        animation: "fade",
        controlNav: false,
        animationSpeed:1500
    });
    $(".jcarouse").jCarouselLite({  /*// Lấy class của ul và gọi hàm jCarouselLite() trong thư viện*/
        vertical: true,				/*// chạy theo chiều dọc*/
        hoverPause:true,			/*// Hover vào nó sẽ dừng lại*/
        visible: 3,					/*// Số bài viết cần hiện*/
        auto:20000,					/*// Tự động scroll*/
        speed:1000	,				/*// Tốc độ scroll*/
        btnNext: '.next',
        btnPrev: '.prev'
    });
	/*
	Header Fixed
	*/
	$(window).bind('scroll', function () {
		var windowSize = $(window).width();
		if( windowSize > 979 ) {
			if ($(this).scrollTop() > 150) {
				$('#sticky-header').removeClass('hide').animate({top:'0px'}, 300);
				$('#sticky-header .sticky-open').removeClass('active');
			} else {
				$('#sticky-header').addClass('hide');
				$('#sticky-header .sticky-open').removeClass('active');
			}
		}
		return false;
	});
	$('.sticky-open').click(function () {
		$('.sticky-form').slideToggle('normal').focus();
		$(this).toggleClass('active');
	});


    /*
	Menu Nav
	*/
	$('ul#left-primary li, ul#sticky-menu-left li').on({
		mouseenter: function() {
/*//            $(this).css("z-index",1000);*/
            $(this).addClass('active');
			$(this).children('ul').stop(true, true).slideDown(400);
			$(this).children('div').stop(true, true).slideDown(400);
		},
		mouseleave: function() {
            $(this).removeClass('active');
			$(this).children('ul').slideUp(100);
			$(this).children('div').slideUp(100);
		}
	});
	
	var desktopmenu = $('ul#primary-menu');
	$('<div id="mobile-container"><ul id="mobile-menu" class="mobile-menu"></ul></div>').insertAfter( desktopmenu );
	$('ul#mobile-menu').html(desktopmenu.children().clone());
	
	$('#toggle-menu').on('click', function(e) {
		e.preventDefault();
		$('#mobile-menu').slideToggle();
		$('#mobile-menu .sub-menu').slideUp();
	});
	
	$('ul#mobile-menu li.menu-parent-item > a').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).next().slideToggle();
		$(this).parent().siblings().find('a.active').removeClass('active');
		$(this).parent().siblings().find('.sub-menu').slideUp();
	});
    var endDate = $( "div.time-counter" ).text()+" 00:00:00";
    $('.getting-married-date-count .number').countdown({
        date: endDate,
        render: function(data) {
            $(this.el).html("<ul class='inline'><li>" + this.leadingZeros(data.days, 2) + "</li><li>:</li><li>" + this.leadingZeros(data.hours, 2) + "</li><li>:</li><li>" + this.leadingZeros(data.min, 2) + "</li><li>:</li><li>" + this.leadingZeros(data.sec, 2) + "</li></ul>");
        }
    });

    /*
	Back to Top
	*/
    $(window).bind('scroll', function () {
        if ($(this).scrollTop() > 200) {
            $('.scrollTop').fadeIn();
        } else {
            $('.scrollTop').fadeOut();
        }
    });

    $('.scrollTop').click(function (e) {
        e.stopPropagation();
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    /*
	PrettyPhoto
	*/
    $('.modal').bind('hide', function () {
        var iframe = $(this).find('iframe');
        var src = iframe.attr('src');
        iframe.attr('src', '');
        iframe.attr('src', src);
    });

    $("a[data-rel^='prettyPhoto']").prettyPhoto({
        show_title: true,
        social_tools: false
    });
    /*
	Zoom Hover
	*/
    $('#filters li a').click(function () {
        $('#filters li').removeClass('active');
        $(this).parent().addClass('active');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });
    /*
	Portfolio List
	*/

    var $container = $('#ourHolder');
    /*// initialize isotope*/
    $container.isotope({
        animationEngine: 'best-available',
        animationOptions: {
            duration: 300,
            easing: 'easeInOutQuad',
            queue: false
        }
    });
   /* // Isotope Chrome Fix*/
    setTimeout(function () {
        $container.isotope('reLayout');
    }, 1000);

    /*
	Shortcode Tabs
	*/
    $('.st-tabs .tab-content').hide().filter(':first').show();
    $('.st-tabs .tab-title li').filter(':first').addClass('active');
    $('.st-tabs .tab-title a').click(function (e) {
        e.preventDefault();
        $(this).parents('li').addClass('active').siblings('li.active').removeClass('active');
        var tab_id = $(this).parents('li').index();
        $(this).parents('.st-tabs').find('.tab-content').hide().siblings('.tab-content').eq(tab_id).fadeIn();
        return false;
    });


    /*
	Shortcode Toggle
	*/
    $('.st-accordion > .accordion-content').hide().filter('.open').show();
    $(".st-accordion > .accordion-title").click(function (e) {
        e.preventDefault();
        var icons = $(this);
        var toggle = $(this).next();
        $(toggle).slideToggle('slow', function () {
            $(icons).toggleClass('active', $(this).is(':visible'));
        });
    });
    var couple_slide= false,
        couple_slide_l=false,
        couple_slide_l_v=false;
    $(".beau-couple-slide .couple-slide-left .couple-img").click(function(e) {
        e.preventDefault();
        if(couple_slide==false){
            $('.couple-slide-left .couple-img').transition({ x: '-200%' }, 1200);
            couple_slide=true;
        }
        else{
            $('.couple-slide-left .couple-img').transition({ x: '0' },3200);
            couple_slide=false;
        }
    }).mouseenter(
        function(e) {
            e.preventDefault();
            if(couple_slide==false){
                $('.couple-slide-left .couple-img').transition({ x: '-200%' }, 1200);
                couple_slide=true;
            }
            else{
                $('.couple-slide-left .couple-img').transition({ x: '0' },3200);
                couple_slide=false;
            }
        }
        );
    $(".beau-couple-slide .couple-slide-right .couple-img").click(function(e) {
        e.preventDefault();
        if(couple_slide_l==false){
            $('.couple-slide-right .couple-img').transition({ x: '200%' }, 1200);
            couple_slide_l=true;
        }
        else{
            $('.couple-slide-right .couple-img').transition({ x: '0' },3200);
            couple_slide_l=false;
        }
    }).mouseenter(function(e) {
            e.preventDefault();
            if(couple_slide_l==false){
                $('.couple-slide-right .couple-img').transition({ x: '200%' }, 1200);
                couple_slide_l=true;
            }
            else{
                $('.couple-slide-right .couple-img').transition({ x: '0' },3200);
                couple_slide_l=false;
            }
        });
    $(" .couple-slide-right .couple-close ").click(function(e) {
        if(couple_slide_l==true){
            $('.couple-slide-right .couple-img').transition({ x: '0' },800);
            couple_slide_l=false;
        }
    });
    $(" .couple-slide-left .couple-close ").click(function(e) {
        if(couple_slide==true){
            $('.couple-slide-left .couple-img').transition({ x: '0' },800);
            couple_slide=false;
        }
    });
    function couple_close() {
        if(couple_slide_l==true){
            $('.couple-slide-right .couple-img').transition({ x: '0' },800);
            couple_slide_l=false;
        }
        if(couple_slide==true){
            $('.couple-slide-left .couple-img').transition({ x: '0' },800);
            couple_slide=false;
        }
    }
    /*
	Shortcode Accordion
	*/
    var allPanels = $('.st-accordion li .accordion-content').hide();
    $('.st-accordion li .open').show();
    $('.st-accordion li .accordion-title').click(function (e) {
        e.preventDefault();
        $(".st-accordion li .accordion-title:before").css("content", "\f068");
        var icons = $(this);
        var accordion = $(this).next();
        if (!icons.hasClass('active')) {
            allPanels.prev().removeClass('active');
            allPanels.slideUp();
            icons.addClass('active');
            accordion.slideDown();
        }
        return false;
    });
    /*
	Shortcode Progress Bar
	*/
	function st_progressbar() {
		$('.st-progress .bar:in-viewport').each(function(e) {
			var currentBar = $(this);
			var w = currentBar.data('width');
			if (!currentBar.hasClass('st-scrollbar')) {
				currentBar.addClass('st-scrollbar');
				currentBar.animate({width: w + '%'}, 500);
			}
		});
	}
	/*
	Shortcode Animated
	*/
	function st_animated() {
		$(".beau-animated:in-viewport").each(function (e) {
			var animated = $(this);
			if (!animated.hasClass('beau-viewport')) {
				setTimeout(function () {
					animated.addClass('beau-viewport');
				}, 200 * e);
			}
		});
	}
	st_animated();
    var is_isotope= false;
	$(window).scroll(function () {
		if(is_isotope==false){
            $container.isotope('reLayout');
            is_isotope=true;
        }
        st_animated();
        couple_close();
	});
    $(".gallery .row-fluid .span2").mouseenter(function(e) {
        e.preventDefault();
       $(this).css("z-index",50);
    });
    $(".gallery .row-fluid .span2").mouseleave(function(e) {
        e.preventDefault();
        $(this).css("z-index",0);
    });
    $(".gallery .row-fluid .span3").mouseenter(function(e) {
        e.preventDefault();
        $(this).css("z-index",50);
    });
    $(".gallery .row-fluid .span3").mouseleave(function(e) {
        e.preventDefault();
        $(this).css("z-index",0);
    });
    $(".question").mouseenter(function(e){
        $(".question-border").css({'top':0, 'left':0, 'right':0, 'bottom':0});
    }).mouseleave(function(e){
        $(".question-border").css({'top':6, 'left':-6, 'right':6, 'bottom':-6});
        });
    $("#commentform #author").attr("placeholder", "Name");
    $("#commentform #email").attr("placeholder", "Email");
    $("#commentform #comment").attr("placeholder", "Messenger");

    fluidvids.init({
        selector: ['iframe'],
        players: ['www.youtube.com', 'player.vimeo.com']
    });
});

function fbs_click(width, height,link) {
    var leftPosition, topPosition;
    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
    u=location.href;
    t=document.title;
    window.open(link+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer', windowFeatures);
    return false;
}
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
