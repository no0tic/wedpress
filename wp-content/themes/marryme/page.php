<?php get_header(); ?>
	<?php
        $style=of_get_option('blog_style');
        $part="style-1";
        switch ($style):
            case 1:
                $part="style-1";
                break;
            case 2:
                $part="style-2";
                break;
            break;
        endswitch;
        $page_template = get_post_meta($post->ID, '_page_template', TRUE);
        switch ($page_template):
            case 'home':
                get_template_part( $part.'/temp', 'home' );
            break;
            case 'event':
                get_template_part($part.'/temp', 'event' );
            break;
            case 'event-detail':
                get_template_part( $part.'/temp', 'event-detail' );
            break;
            case 'blog':
                get_template_part( $part.'/temp', 'blog' );
            break;
            case 'blog-full':
                get_template_part( $part.'/temp', 'blog-full-layout' );
            break;
            case 'blog-left':
                get_template_part( $part.'/temp', 'blog-left-sidebar' );
            break;
            case 'blog-right':
                get_template_part( $part.'/temp', 'blog-right-sidebar' );
            break;
            case 'blog-detail':
                get_template_part( $part.'/temp', 'blog-detail' );
            break;
            case 'gallery':
                get_template_part( $part.'/temp', 'gallery' );
            break;
            case 'contact':
                get_template_part( $part.'/temp', 'contact' );
            break;
            case 'guest-book':
                get_template_part( $part.'/temp', 'guest-book' );
            break;
            case 'shortcode':
                get_template_part( '404' );
            break;
        endswitch;
	?>
<?php get_footer(); ?>