<?php get_header(); ?>
<?php
$postType= get_post_type();
$style=of_get_option('blog_style');
$part="style-1";
switch ($style):
    case 1:
        $part="style-1";
        break;
    case 2:
        $part="style-2";
        break;
endswitch;
switch ($postType):
    case 'event':
        get_template_part( $part.'/temp-event-detail');
        break;
    case 'post':
        get_template_part( $part.'/temp-blog-detail');
        break;
endswitch;
?>
<?php get_footer(); ?>