<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 7/16/14
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
$couple =   get_post_meta(get_the_ID(), '_page_couple', TRUE);
$time =   get_post_meta(get_the_ID(), '_page_time', TRUE);
$time_count = get_post_meta(get_the_ID(), '_page_time_count', TRUE);
$date = strtotime($time_count);
$event =    get_post_meta(get_the_ID(), '_page_event', TRUE);
$tweet =    get_post_meta(get_the_ID(), '_page_tweet', TRUE);
$album =    get_post_meta(get_the_ID(), '_page_album', TRUE);
$blog =     get_post_meta(get_the_ID(), '_page_blog', TRUE);
$attending =     get_post_meta(get_the_ID(), '_page_attending', TRUE);
$id=get_the_ID();

if (have_posts()) :	while (have_posts()) : the_post();
if(!empty($couple)){
    echo do_shortcode('[couple]');
}
if(!empty($time)){
    ?>
<section class="beau-getting-married beau-animated fade-in">
    <h2 class="getting-married"><?php echo get_post_meta(get_the_ID(), '_page_time_text', TRUE);?></h2>
    <div class="getting-married-date "><?php echo date( 'l', $date ).' '.date( 'j', $date ).' '.date( 'F ', $date ).' '.date( 'Y ', $date ); ?></div>
    <div class="getting-married-date-count">
        <div class="hide time-counter"><?php echo $time_count;?></div>
       <div class="number">
       </div>
       <div class="time-text"><ul class="inline"><li>days</li><li>hours</li><li>minutes</li><li>seconds</li></ul></div>
    </div>
</section>
<?php }
    if((!empty($event) && empty($tweet)) || (empty($event) && !empty($tweet))){
        $class="span12";
    }else if(!empty($event) && !empty($tweet)){
        $class="span6";
    }
if(!empty($class)){
    ?>
<section class="beau-event-tweet">
    <div class="row-fluid">
        <?php if($class=="span12" && !empty($event)){?>
            <div class="<?php echo $class;?> home-event beau-animated move-to-right">
                <?php echo do_shortcode('[event item_number=99]');?>
            </div>
        <?php }else if($class=="span12" && !empty($tweet)){?>
            <div class="<?php echo $class;?> home-tweet-follow beau-animated move-to-left">
                <?php echo do_shortcode('[last_tweets class="span6"]');?>
            </div>
        <?php }else if($class=="span6"){
            echo '<div class="'.$class.' home-event beau-animated move-to-right">';
            echo do_shortcode('[event item_number=99]');
            echo '</div>';
            echo '<div class="'.$class.' home-tweet-follow beau-animated move-to-left">';
            echo do_shortcode('[last_tweets]');
            echo '</div>';
        }?>
    </div>
</section>
<?php
    }
    if(!empty($album)){
        echo do_shortcode('[album item_number="99"]');
    }
    if(!empty($blog)){
        echo do_shortcode('[blog item_number=4]');
    }
    if(!empty($attending)){
        echo do_shortcode('[attendingform]');
    }
   endwhile; endif
?>