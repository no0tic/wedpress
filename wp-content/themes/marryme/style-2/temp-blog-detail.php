<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 7/22/14
 * Time: 12:44 AM
 * To change this template use File | Settings | File Templates.
 */ ?>

<section class="beau-blog">
    <div class="row-fluid beau-blog-detail">
        <div class="span1">
            <div class="date"><span><?php echo get_the_time('d') ?></span></div>
            <div class="month-year"><span><?php echo substr( get_the_time('F'), 0, 3 ).' '.get_the_time('Y');?></span></div>
        </div>
        <div class="span8">
            <h3><?php the_title();?></h3>
            <div class="post-thumb">
                <?php $featuredID =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                      $blog_location = get_post_meta(get_the_ID(), '_blog_location', TRUE);
                if(!empty($featuredID)){
                ?>
                <img src="<?php echo $featuredID[0]; ?>" alt="<?php the_title();?>">
                <?php }?>
            </div>
            <div class="full-description">
                <?php echo $blog_location;?>
            </div>
            <div class="blog-meta">
                <span class="blog-comment"><i class="icon-comment"></i><?php comments_number( '0 comment', '1 comment', '% comments' );?></span>
                                <span class="blog-tags">
                                    <?php $posttags = get_the_tags();
                                    the_tags('<i class="icon-tags"></i>',', ');
                                    ?>
                                </span>
            </div>
            <div class="content">
                <?php
                global $post;
                echo $post->post_content;
                ?>

            </div>
            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 2,
            );
            $loop = new WP_Query($args);
            ob_start();
            if ($loop->have_posts()):
            ?>
            <div class="last-test">
                <ul>
                    <?php while ($loop->have_posts()) : $loop->the_post();?>
                        <li><i class="icon-angle-right"></i><span><a href="<?php get_permalink();?>"><?php the_title();?></a></span></li>
                    <?php endwhile;?>
                </ul>
            </div>
            <?php endif;?>
            <div class="row-fluid comment">
                <?php comments_template( '/short-comments.php' ); ?></div>
        </div>
        <div class="span3">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar blog')) : ?><?php endif; ?>
        </div>
    </div>
</section>