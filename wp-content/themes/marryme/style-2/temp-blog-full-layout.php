<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 7/22/14
 * Time: 12:44 AM
 * To change this template use File | Settings | File Templates.
 */

global $imgsize, $loop;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 6,
    'paged' => $paged,
);
$loop = new WP_Query($args);
ob_start();
?>
<section class="beau-blog">
    <div class="blog-title">OUR WEDDING BLOG</div>
    <div class="blog-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</div>
    <?php
        if ($loop->have_posts()):?>
    <div class="blog-lists blog-lists-full-layout">
        <?php $i=0; while ($loop->have_posts()) : $loop->the_post();
            $featuredID =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
            $blog_location = get_post_meta(get_the_ID(), '_blog_location', TRUE);?>
        <div class="row-fluid">
            <div class="blog-list-container">
                <div class="span12 beau-animated fade-in">
                    <div class="full-content">
                        <a href="<?php echo get_permalink()?>">
                        <h3><?php the_title();?></h3>
                        <div class="post-thumb hover-img">
                            <img src="<?php echo $featuredID[0]?>" alt="<?php echo get_the_title();?>">
                            <div class="image-op"></div>
                            <div class="odd">
                                <figure class="effect-bubba">
                                    <figcaption>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        </a>
                        <div class="full-description">
                            <?php echo $blog_location;?>
                        </div>
                        <div class="blog-meta">
                            <span class="date"><?php echo get_the_date();?></span>
                            <span class="blog-comment"><i class="icon-comment"></i><?php comments_number( '0 comment', '1 comment', '% comments' );?></span>
                            <span class="blog-tags">
                            <?php $posttags = get_the_tags();
                                the_tags('<i class="icon-tags"></i>',', ');
                            ?>
                            </span>
                        </div>
                        <div class="content">
                           <?php beau_excerpt(80, true);?>
                        </div>
                        <div class="blog-social">
                            <ul class="inline">
                                <li><a onClick="return fbs_click(600, 500,'http://www.facebook.com/sharer.php?u=')" title="Facebook" target="_blank"><i class="icon-facebook"></i></a></li>
                                <li><a onClick="return fbs_click(600, 500,'https://twitter.com/share?url=')" title="Twitter" target="_blank"><i class="icon-twitter"></i></a></li>
                                <li><a onClick="return fbs_click(600, 500,'http://www.linkedin.com/shareArticle?mini=true&url=')" title="Linkin" target="_blank"><i class="icon-linkedin"></i></a></li>
                                <li><a onClick="return fbs_click(600, 500,'https://plusone.google.com/_/+1/confirm?hl=en&url=')" title="Google+" target="_blank"><i class="icon-google-plus"></i></a></li>
<!--                                <li><a href="#"</a></li>-->
                                <li class="blog-author">/ <?php echo get_the_author();?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $i++;  endwhile;
        ?>
    </div>
     <div class="bg-icon-cut"></div>
    <?php beau_pagenav(); endif;?>
</section>