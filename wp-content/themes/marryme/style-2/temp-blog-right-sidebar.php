<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 7/22/14
 * Time: 12:44 AM
 * To change this template use File | Settings | File Templates.
 */
global $imgsize, $loop;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 6,
    'paged' => $paged,
);
$loop = new WP_Query($args);
?>
<section class="beau-blog">
    <div class="row-fluid beau-blog-detail beau-blog-right-side">
        <div class="span9">
            <?php
            ob_start();
            if ($loop->have_posts()):?>
            <div class="row-fluid">
                <?php $i=0; while ($loop->have_posts()) : $loop->the_post();
                    $featuredID =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail');
                    $blog_location = get_post_meta(get_the_ID(), '_blog_location', TRUE);?>
                <div class="span2">
                    <div class="date"><span><?php echo get_the_time('d') ?></span></div>
                    <div class="month-year"><span><?php echo substr( get_the_time('F'), 0, 3 ).' '.get_the_time('Y');?></span></div>
                </div>
                <div class="span10">
                        <div class="full-content">
                            <a href="<?php echo get_permalink()?>" alt="<?php the_title();?>"><h3><?php the_title();?></h3></a>
                            <div class="post-thumb hover-img">
                                <a href="<?php echo get_permalink()?>" alt="<?php the_title();?>">
                                    <img src="<?php echo $featuredID[0]; ?>">
                                    <div class="image-op"></div>
                                    <div class="odd">
                                        <figure class="effect-bubba">
                                            <figcaption>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </a>
                            </div>
                            <div class="full-description">
                                <?php echo $blog_location;?>
                                <div><span>By <?php echo get_the_author();?></span><span>|</span><span><?php echo get_the_time('d').' '.get_the_time('F').' '.get_the_time('Y'); ?></span></div>
                            </div>
                            <div class="blog-meta">
                                <span class="blog-comment"><i class="icon-comment"></i><?php comments_number( '0 comment', '1 comment', '% comments' );?></span>
                                <span class="blog-tags">
                                    <?php $posttags = get_the_tags();
                                        the_tags('<i class="icon-tags"></i>',', ');
                                    ?>
                                </span>
                            </div>
                            <div class="content">
                                <?php beau_excerpt(80, true);?>
                            </div>
                        </div>
                    </div>
            <?php endwhile;
                beau_pagenav();
                ?>
            </div>
            <?php endif;?>
        </div>
        <div class="span3 widget-blog">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar blog')) : ?><?php endif; ?>
        </div>
    </div>
</section>