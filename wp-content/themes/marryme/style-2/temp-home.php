<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Minh Tuy
 * Date: 7/16/14
 * Time: 9:57 PM
 * To change this template use File | Settings | File Templates.
 */
$couple =   get_post_meta(get_the_ID(), '_page_couple', TRUE);
$time =   get_post_meta(get_the_ID(), '_page_time', TRUE);
$time_count = get_post_meta(get_the_ID(), '_page_time_count', TRUE);
$date = strtotime($time_count);
$event =    get_post_meta(get_the_ID(), '_page_event', TRUE);
$tweet =    get_post_meta(get_the_ID(), '_page_tweet', TRUE);
$album =    get_post_meta(get_the_ID(), '_page_album', TRUE);
$blog =     get_post_meta(get_the_ID(), '_page_blog', TRUE);
$attending =     get_post_meta(get_the_ID(), '_page_attending', TRUE);
$id=get_the_ID();

if (have_posts()) :	while (have_posts()) : the_post();
if(!empty($couple)){
    echo do_shortcode('[couple]');
}
if(!empty($time)){
?>
<section class="beau-getting-married style2 beau-animated fade-in">
    <h2 class="getting-married hide"><?php echo get_post_meta(get_the_ID(), '_page_album_text', TRUE);?></h2>
    <div class="getting-married-date-count">
        <div class="hide time-counter"><?php echo $time_count;?></div>
       <div class="number">
       </div>
       <div class="time-text style-2"><ul class="inline"><li>days</li><li>hours</li><li>minutes</li><li>seconds</li></ul></div>
    </div>
    <div class="getting-married-date style-2"></div>
</section>
<?php }
    if(!empty($tweet)){?>
    <section class="beau-event-tweet style2">
        <div class="row-fluid">
            <div class="span12 home-tweet-follow beau-animated move-to-right">
                <?php echo do_shortcode('[tweets_style2]');?>
            </div>
        </div>
    </section>
    <?php }
        if(!empty($event)){
    ?>
<section class="beau-event-tweet event style2">
    <div class="row-fluid">
            <div class="span12 home-event">
                <?php echo do_shortcode('[event_style2 item_number=99]');?>
            </div>
    </div>
</section>
<?php
    }
    if(!empty($album)){
        echo do_shortcode('[album_style2 item_number="99"]');
    }
    if(!empty($blog)){
        echo do_shortcode('[blog_style2 item_number=4]');
    }
    if(!empty($attending)){
        echo do_shortcode('[attendingform_style2]');
    }
   endwhile; endif
?>