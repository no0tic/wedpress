<div class="beau-search-result">
    <?php
if (have_posts()) : ?>
    <?php
    while (have_posts()) : the_post();
	?>
	<!-- post entry -->
	<article id="post-<?php the_ID(); ?>" <?php post_class('search-list clearfix'); ?>>
		<h3 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php echo strip_tags(get_the_title()); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
		<div class="post-excerpt">
			<?php beau_excerpt(100,true); ?>
		</div>			
	</article>
	<!-- end post entry -->
	<?php
        endwhile;
    else:
	?>
    <!-- post entry -->
    <article class="post-single clearfix">
        <h3 class="post-title"><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'slicetheme'); ?></h3>
    </article>

    <?php
endif; ?>
</div>